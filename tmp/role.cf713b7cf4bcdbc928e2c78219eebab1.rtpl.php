<?php if(!class_exists('raintpl')){exit;}?><?php $tpl = new RainTPL;$tpl_dir_temp = self::$tpl_dir;$tpl->assign( $this->var );$tpl->draw( dirname("settings") . ( substr("settings",-1,1) != "/" ? "/" : "" ) . basename("settings") );?>

<div id="bottom_scroll" class="set">
	<a href="settings/redefine/menus"><< <?php echo strtoupper( $back );?></a>
	<div id="set">
	<form method="POST">
		<span class="roleset">/*----<?php echo $role_name;?>----*/</span>
		<div class="middle_set">
			<table>
				<tr>
					<th><?php echo $nameset;?></th>
					<th><?php echo $order;?></th>
					<th><?php echo $englishset;?></th>
					<th><?php echo $latvianset;?></th>
					<th><?php echo $visible;?></th>
				</tr>
				<?php $counter1=-1; if( isset($menus) && is_array($menus) && sizeof($menus) ) foreach( $menus as $key1 => $value1 ){ $counter1++; ?>	
				<tr>
					<td>
						<input class="set_name" type="text" readonly="readonly" value="<?php echo $value1["name"];?>" name="name_<?php echo $value1["id"];?>" />
					</td>
					<td>
						<input class="set_order" type="text" value="<?php echo $value1["order"];?>" name="order_<?php echo $value1["id"];?>" autocomplete="off" />
					</td>
					<td>
						<input class="set_lang" type="text" value="<?php echo $value1["english"];?>" name="english_<?php echo $value1["id"];?>" autocomplete="off" />
					</td>
					<td>
						<input class="set_lang" type="text" value="<?php echo $value1["latvian"];?>" name="latvian_<?php echo $value1["id"];?>" autocomplete="off" />
					</td>
					<td>
						<?php if( $value1["visible"] == 1 ){ ?>

							<input type="checkbox" checked="true" name="visible_<?php echo $value1["id"];?>" />
						<?php }else{ ?>

							<input type="checkbox" name="visible_<?php echo $value1["id"];?>"/>
						<?php } ?>

					</td>
				</tr>
				<?php } ?>

			</table>
		</div>
		<input type="hidden" name="<?php echo $ext_name;?>" value="<?php echo $ext_s;?>" />
		<input type="hidden" name="<?php echo $action_name;?>" value="<?php echo $action_s;?>" />
		<input type="hidden" name="ids" value="<?php echo $ids;?>" />
		<input type="submit" value="<?php echo $submit;?>" class="radd" />
	</form>
	</div>
</div>