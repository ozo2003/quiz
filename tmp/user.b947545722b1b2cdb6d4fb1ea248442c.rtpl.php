<?php if(!class_exists('raintpl')){exit;}?><?php $tpl = new RainTPL;$tpl_dir_temp = self::$tpl_dir;$tpl->assign( $this->var );$tpl->draw( dirname("manage") . ( substr("manage",-1,1) != "/" ? "/" : "" ) . basename("manage") );?>

<div id="bottom_scroll">
	<a href="manage/users"><< <?php echo strtoupper( $back );?></a>
	<?php if( $isid ){ ?>

	<span class="right_text">
		<form method="POST">
			<input type="submit" value="<?php echo strtoupper( $delete );?>" />
			<input type="hidden" name="<?php echo $action_name;?>" value="<?php echo $action2_s;?>" />
			<input type="hidden" name="<?php echo $ext_name;?>" value="<?php echo $ext_s;?>" />
		</form>
	</span>
	<?php } ?>

	<div class="add_set">
		<form method="POST" class="add">
			<label for="username"><?php echo $username;?><span class="obl">*</span></label>
			<?php if( !$isid ){ ?>

				<input type="text" name="<?php echo $username_name;?>" autocomplete="off" id="username" value="<?php echo $username_val;?>" /><br />
			<?php }else{ ?>

				<input type="text" readonly="readonly" class="readonly" autocomplete="off" id="username" value="<?php echo $username_val;?>" /><br />
			<?php } ?>

			<?php if( !$isid ){ ?>

				<label for="password1"><?php echo $password_reg;?><span class="obl">*</span></label>
				<input type="password" name="<?php echo $password1_name;?>" autocomplete="off" id="password1" /><br />
				<label for="password2"><?php echo $again_reg;?><span class="obl">*</span></label>
				<input type="password" name="<?php echo $password2_name;?>" autocomplete="off" id="password2" /><br />
			<?php } ?>

			<label for="name"><?php echo $name;?><span class="obl">*</span></label>
			<input type="text" name="<?php echo $name_name;?>" autocomplete="off" id="name" value="<?php echo $name_val;?>" /><br />
			<label for="surname"><?php echo $surname;?></label>
			<input type="text" name="<?php echo $surname_name;?>" autocomplete="off" id="surname" placeholder="(<?php echo $optional;?>)" value="<?php echo $surname_val;?>" /><br />
			<label for="email"><?php echo $email;?></label>
			<input type="text" name="<?php echo $email_name;?>" autocomplete="off" id="email" placeholder="(<?php echo $optional;?>)" value="<?php echo $email_val;?>" /><br />
			<label for="phone"><?php echo $phone;?></label>
			<input type="text" name="<?php echo $phone_name;?>" autocomplete="off" id="phone" placeholder="(<?php echo $optional;?>)" value="<?php echo $phone_val;?>" /><br />
			<label for="role"><?php echo $role;?></label>
			<select name="<?php echo $role_name;?>" id="role" <?php if( $role_id == 4 || $id_val == $id ){ ?> class="noblock" <?php } ?>>
			<?php $counter1=-1; if( isset($roles) && is_array($roles) && sizeof($roles) ) foreach( $roles as $key1 => $value1 ){ $counter1++; ?>

				<?php if( $value1["level"] != 1 ){ ?>

					<?php if( $value1["id"] == $role_id ){ ?>

						<option value="<?php echo $value1["id"];?>" selected="selected"><?php echo $value1["name"];?></option>
					<?php }else{ ?>

						<option value="<?php echo $value1["id"];?>"><?php echo $value1["name"];?></option>
					<?php } ?>

				<?php } ?>

			<?php } ?>

			</select><br />
			<?php if( $isid && $role_id != 4 && $id_val != $id ){ ?>

				<label for="blocked"><?php echo $blocked;?></label>
				<?php if( $blocked_val == 0 ){ ?>

					<input class="roles_check" type="checkbox" name="<?php echo $blocked_name;?>">
				<?php }else{ ?>

					<input class="roles_check" type="checkbox" name="<?php echo $blocked_name;?>" checked="checked">
				<?php } ?>

			<?php } ?>

			<?php if( $isid ){ ?>

				<div class="ch chone">
					<a href="manage/users/password/<?php echo $ext;?>"><div class="chpuser">&nbsp;<?php echo $chpass;?></div></a>
				</div><br />
				<div class="ch">
					<a href="manage/users/image/<?php echo $ext;?>"><div class="chpuser">&nbsp;<?php echo $primage;?></div></a>
				</div><br />
			<?php } ?>

			<?php if( $isid ){ ?>

				<div class="text addtext"><?php echo $user_edit_warning;?></div>
			<?php } ?>

			<input type="hidden" name="<?php echo $ext_name;?>" value="<?php echo $ext_s;?>" />
			<input type="hidden" name="<?php echo $action_name;?>" value="<?php echo $action_s;?>" />
			<?php if( ($isid && $role_id == 4) || !$isid ){ ?>

				<input type="hidden" name="<?php echo $blocked_name;?>" value="<?php echo $blocked_cry;?>" />
			<?php } ?>

			<input type="submit" value="<?php echo $submit;?>" />
		</form>
	</div>
</div>