<?php if(!class_exists('raintpl')){exit;}?><?php $tpl = new RainTPL;$tpl_dir_temp = self::$tpl_dir;$tpl->assign( $this->var );$tpl->draw( dirname("menu") . ( substr("menu",-1,1) != "/" ? "/" : "" ) . basename("menu") );?>

<div id="reg_top">
<div id="reg"><?php echo $registration;?></div>
<div id="bottom_scroll">
	<div class="add_set">
		<form method="POST" action="actions/add_user.php" class="add">
			<label for="username"><?php echo $username;?></label>
			<input type="text" name="username" autocomplete="off" id="username" /><br />
			<label for="password1"><?php echo $password_reg;?></label>
			<input type="password" name="password1" autocomplete="off" id="password1" /><br />
			<label for="password2"><?php echo $again_reg;?></label>
			<input type="password" name="password2" autocomplete="off" id="password2" /><br />
			<label for="name"><?php echo $name;?></label>
			<input type="text" name="name" autocomplete="off" id="name" /><br />
			<label for="surname"><?php echo $surname;?></label>
			<input type="text" name="surname" autocomplete="off" id="surname" placeholder="(<?php echo $optional;?>)" /><br />
			<label for="email"><?php echo $email;?></label>
			<input type="text" name="email" autocomplete="off" id="email" placeholder="(<?php echo $optional;?>)" /><br />
			<label for="phone"><?php echo $phone;?></label>
			<input type="text" name="phone" autocomplete="off" id="phone" placeholder="(<?php echo $optional;?>)" /><br />
			<label for="code"><?php echo $code_reg;?></label>
			<input type="text" name="code" autocomplete="off" id="code" placeholder="(<?php echo $optional;?>)" /><br />
			<input type="submit" value="<?php echo $register;?>" />
		</form>
	</div>
</div>
</div>