<?php if(!class_exists('raintpl')){exit;}?><?php $tpl = new RainTPL;$tpl_dir_temp = self::$tpl_dir;$tpl->assign( $this->var );$tpl->draw( dirname("manage") . ( substr("manage",-1,1) != "/" ? "/" : "" ) . basename("manage") );?>

<div id="bottom_scroll">
<a href="manage/roles"><< <?php echo strtoupper( $back );?></a>
	<div class="add_set">
		<form method="POST" action="actions/add_role.php" class="add">
			<label for="role"><?php echo $role;?></label>
			<input type="text" name="role" autocomplete="off" id="role" /><br />
			<label for="manager_id"><?php echo $manager;?></label>
			<select name="manager_id" id="manager_id">
				<?php $counter1=-1; if( isset($usersval) && is_array($usersval) && sizeof($usersval) ) foreach( $usersval as $key1 => $value1 ){ $counter1++; ?>

					<option value="<?php echo $value1["id"];?>"><?php echo $value1["name"];?></option>
				<?php } ?>

			</select><br />
			<label for="english"><?php echo $englishset;?></label>
			<input type="text" name="english" autocomplete="off" id="english" /><br />
			<label for="latvian"><?php echo $latvianset;?></label>
			<input type="text" name="latvian" placeholder="Optional" autocomplete="off" id="latvian" /><br />
			<label for="manager"><?php echo $manager;?></label>
			<input type="checkbox" name="manager" id="manager" /><br />
			<label for="active"><?php echo $active;?></label>
			<input type="checkbox" name="active" id="active" /><br />
			<input type="submit" value="<?php echo $submit;?>" />
		</form>
	</div>
</div>