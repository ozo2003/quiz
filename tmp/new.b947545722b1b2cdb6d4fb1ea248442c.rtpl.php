<?php if(!class_exists('raintpl')){exit;}?><?php $tpl = new RainTPL;$tpl_dir_temp = self::$tpl_dir;$tpl->assign( $this->var );$tpl->draw( dirname("manage") . ( substr("manage",-1,1) != "/" ? "/" : "" ) . basename("manage") );?>

<div id="bottom_scroll">
	<a href="manage/users"><< <?php echo strtoupper( $back );?></a>
	<div class="add_set">
		<form method="POST" action="actions/add_user.php" class="add">
			<label for="username"><?php echo $username_reg;?></label>
			<input type="text" name="username" autocomplete="off" id="username" /><br />
			<label for="password1"><?php echo $password_reg;?></label>
			<input type="password" name="password1" autocomplete="off" id="password1" /><br />
			<label for="password2"><?php echo $again_reg;?></label>
			<input type="password" name="password2" autocomplete="off" id="password2" /><br />
			<label for="name"><?php echo $name_reg;?></label>
			<input type="text" name="name" autocomplete="off" id="name" /><br />
			<label for="surname"><?php echo $surname_reg;?></label>
			<input type="text" name="surname" autocomplete="off" id="surname" /><br />
			<label for="email"><?php echo $email_reg;?></label>
			<input type="text" name="email" autocomplete="off" id="email" /><br />
			<label for="phone"><?php echo $phone_reg;?></label>
			<input type="text" name="phone" autocomplete="off" id="phone" /><br />
			<label for="role"><?php echo $role_reg;?></label>
			<select name="role" id="role">
			<?php $counter1=-1; if( isset($roles) && is_array($roles) && sizeof($roles) ) foreach( $roles as $key1 => $value1 ){ $counter1++; ?>

				<option value="<?php echo $value1["id"];?>"><?php echo $value1["value"];?></option>
			<?php } ?>

			</select><br />
			<input type="submit" value="<?php echo $submit;?>" />
		</form>
	</div>
</div>