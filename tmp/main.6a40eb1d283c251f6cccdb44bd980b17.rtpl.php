<?php if(!class_exists('raintpl')){exit;}?><!DOCTYPE html>
<html>
	<head>
		<base href="<?php echo $base_url;?>">
		<link rel="stylesheet" type="text/css" href="templates/css/requiz_style_<?php echo $theme;?>.css">
		<title><?php echo $selfpage;?> [<?php echo $timer;?> sec]</title>
		<link rel="shortcut icon" type="image/png" href="templates/favicon.png" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!--<script type="text/javascript" src="templates/js/jquery.js"></script>
		<script type="text/javascript" src="templates/js/migrate.js"></script>
		<script type="text/javascript" src="templates/js/jquery.form.js"></script>
		<script type="text/javascript" src="templates/js/requiz_script.js"></script>-->
	</head>
	<body>
		<div id="main">
			<?php echo $content;?>
		</div>
	</body>
</html>