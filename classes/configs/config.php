<?php

/*Static*/
date_default_timezone_set("Europe/Riga");
error_reporting(E_ALL | E_STRICT); 
ini_set("session.gc_maxlifetime", "7200");
ini_set("display_errors", 1); 
define("ROOT", "/");
define("SYSTEM_ID", 1);
define("API_URL", "http://quiz.api/api.php");
define("SALT", substr(sha1(API_URL),0,30));
define("DEFINEFOLDER", "defines/");
define("EXPLODE", 0);

/*Dynamic*/
define("IP", $_SERVER["REMOTE_ADDR"]);
define("LANG", API_Define::getLangId());
define("HOST", Functions::Host());
define("ROLE", User::loggedUserRoleId());
define("IS_MANAGER", User::isManager());
define("IS_ADMIN", User::isAdmin());
define("LEVEL", User::loggedUserLevel());
define("THEME", "default");