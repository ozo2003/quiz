<?php

class User {

	const UPDIR = "upload/users/";
	const EXTS = 'jpg,jpeg,gif,png,bmp,tif,tiff,webm,php';

	public static function Login($user,$pass){
		unset($_SESSION['user']);
		$pass = sha1($pass);
		$result = API_User::checkUser($user,$pass);
		if($result){
			$role = API_Role::checkUserRole($result['user_id']);
			$result += $role;
			if($result['role_id'] == 4){
				API_User::setAdminIp();
			}
			Functions::fillSession($result, 'user');
			return TRUE;
		}
		return FALSE;
	}

	public static function loggedUserLevel(){
		if(isset($_SESSION['user']['level'])){
			return $_SESSION['user']['level'];
		} else {
			return 1;
		}
	}

	public static function userData(){
		return $_SESSION['user'];
	}

	public static function loggedUserRoleId(){
		if(isset($_SESSION['user']['role_id'])){
			return $_SESSION['user']['role_id'];
		} else {
			return 1;
		}
	}

	public static function isManager(){
		return (in_array(self::loggedUserLevel(), array(3,4)) ? 1 : 0);
	}

	public static function isAdmin(){
		return (self::loggedUserLevel() == 4 ? 1 : 0);
	}

	public static function loggedUserName(){
		if(isset($_SESSION['user'])){
			return $_SESSION['user']['name']." ".$_SESSION['user']['surname'];
		} else {
			return API_Define::Translate('guest');
		}
	}

	public static function loggedUserId(){
		if(isset($_SESSION['user'])){
			return $_SESSION['user']['user_id'];
		} else {
			return FALSE;
		}
	}

	public static function setImageUpload($user_id){
		if(Input::post('submit')){
			self::imageUpload($user_id);
			return;
		}
		if(Input::post('revoke')){
			self::revokeImage($user_id);
			return;
		}
	}

	public static function revokeImage($user_id){
		API_User::revokeUserImage($user_id);
		return;
	}

	private static function getDimensionsUser($file, $max = 184){
		$data = getimagesize($file);
		$ratio = Functions::getRatio($data[0],$data[1]);
		if($ratio == 1) {
			$w = $max;
			$h = $max;
		} elseif($ratio > 1) {
			$w = $max;
			$h = $max / $ratio;
		} else {
			$h = $max;
			$w = $max * $ratio;
		}
		return array(
			'width' => $data[0],
			'height' => $data[1],
			'newwidth' => $w,
			'newheight' => $h
		);
	}

	private static function imageUpload($user_id){
		$file_file = Input::file('file_file',NULL);
		$file_url = Input::post('file_url',NULL);

		if($file_file && !$file_file['error']){
			$name = self::uploadImage($file_file);
			if($name){
				API_User::setUserImage($user_id,$name);
				return;
			}
			return;
		}
		if($file_url){
			$name = self::writeImage($file_url);
			if($name){
				API_User::setUserImage($user_id,$name);
				return;
			}
			return;
		}
	}

	private static function upName($file, $ext){
		return self::UPDIR.$file.'.'.$ext;
	}

	private static function uploadImage($file_file){
		$file_exists = self::scanDir($file_file['tmp_name']);
		if($file_exists){
			return $file_exists;
		}
		$allowed = explode(",",self::EXTS);
		$ext = explode('.', $file_file['name']);
		$ext = end($ext);
		$name = self::upName(md5_file($file_file['tmp_name']), $ext);
		if(in_array($ext, $allowed)) {
			if($file_file['size'] < (int)(ini_get('upload_max_filesize'))*2^20){
				move_uploaded_file($file_file['tmp_name'], $name);
				$dim = self::getDimensionsUser($name);
				exec('convert '.$name.' -resize '.$dim['newwidth'].'x'.$dim['newheight'].' -quality 100 '.$name);
				return $name;
			}
			return FALSE;
		}
		return FALSE;
	}

	private static function writeImage($file_url){
		$file_exists = self::scanDir($file_url);
		if($file_exists){
			return $file_exists;
		}
		$ext = explode(".",$file_url);
		$ext = end($ext);
		$allowed = explode(",",self::EXTS);
		if(in_array($ext, $allowed)) {
			$file = fopen($file_url,"rb");
			$name = self::upName(md5_file($file_url), $ext);
			if($file){
				$new = fopen($name,"wb");
				if($new){
					while(!feof($file)){
						fwrite($new,fread($file,1024*8),1024*8);
					}
					fclose($new);
				}
				fclose($file);
				return $name;
			}
			return FALSE;
		}
		return FALSE;
	}

	private static function scanDir($name, $dir = self::UPDIR){
		$is = glob($dir.md5_file($name).'.{'.self::EXTS.'}',GLOB_BRACE);
		return (empty($is) ? FALSE : $is[0]);
	}

	public static function changeUserPassword($user_id,$old,$new,$new2){
		if(!$old || !$new || !$new2 || $old == $new || $new != $new2){
			return "/password/".$user_id;
		}
		API_User::changeUserPassword($user_id,$old,$new);
		return "";
	}

	public static function saveUser($user_id = 0){
		$data = array(
			'name' => Input::post(__('name')),
			'surname' => Input::post(__('surname')),
			'email' => (Functions::checkEmail(Input::post(__('email'))) ? Input::post(__('email')) : NULL),
			'phone' => Input::post(__('phone'))
		);
		if($user_id){
			$data2 = array(
				'user_id' => $user_id,
				'blocked' => Input::post(__('blocked'), NULL, 1)
			);
			$data += $data2;
			if(!$data['name']){
				return FALSE;
			}
			API_User::editUser($data, Input::post(__('role'),2));
		} else {
			$data2 = array(
				'username' => Input::post(__('username')),
				'password' => (Input::post(__('password1')) == Input::post(__('password2')) && Input::post(__('password1')) ? sha1(Input::post(__('password'))) : NULL),
				'creator_id' => self::loggedUserId()
			);
			$data += $data2;
			$must = array('username','password','name');
			foreach($must AS $m){
				if(!$data[$m]){
					return FALSE;
				}
			}
			var_dump($data);
			API_User::addUser($data,Input::post(__('role'),2));
		}
	}

	public static function deleteUser($user_id){
		API_User::deleteUser($user_id);
	}

}