<?php

class Define {

	public static function rebuildLoopValues(){
		for($i=1;$i<=2;$i++){
			$data = "<?php\n\n\$loop = array(\n";
			foreach(API_Define::getLoopValues($i) AS $loop){
				$data .= "    array('name'=>'".$loop['name']."','value'=>'".str_replace("'","&#039;",$loop['value'])."'),\n";
			}
			$data = substr($data,0,-2);
			$data .= "\n);\n";
			file_put_contents(DEFINEFOLDER.$i."/loop.php",$data);
		}
	}

	public static function getLoopValues($lang_id = LANG){
		$filename = DEFINEFOLDER.$lang_id."/loop.php";
		if(is_file($filename)){
			$defines = preg_replace('/(^<\?(php)?(\s|\n)+|(\? >))/i', '', file_get_contents($filename));
			eval($defines);

			return $loop;
		}
		return API_Define::getLoopValues($lang_id);
	}

	public static function rebuildHelpValues(){
		for($i=1;$i<=2;$i++){
			$data = "<?php\n\n\$help = array(\n";
			foreach(API_Define::getTextNames() AS $key => $name){
				$data .= "    array('name'=>'".$name['name']."','value'=>'".str_replace("'","&#039;",API_Define::getText($name['name'],$i))."'),\n";
			}
			$data = substr($data,0,-2);
			$data .= "\n);\n";
			file_put_contents(DEFINEFOLDER.$i."/help.php",$data);
		}
	}

	public static function getText($name, $lang_id = LANG){
		$filename = DEFINEFOLDER.$lang_id."/help.php";
		if(is_file($filename)){
			$defines = preg_replace('/(^<\?(php)?(\s|\n)+|(\? >))/i', '', file_get_contents($filename));
			eval($defines);

			foreach($help AS $data){
				if($data['name'] == $name){
					return $data['value'];
				}
			}
		}
		return API_Define::getText($name);
	}

	public static function Translate($name){
		$loop = self::getLoopValues();
		foreach($loop AS $l){
			if($l['name'] == $name){
				return $l['value'];
			}
		}
		return $name;
	}

}