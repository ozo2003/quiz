<?php

class SetPage {

	public static $getpage = NULL;
	public static $fullpage;
	public static $getpageid;
	public static $selfpage;
	public static $menu;

	public static function Post(){
		$action = Crypt::decrypt(Input::post(__("action")));
		$link = (Input::post(__("page"),NULL) ? Crypt::decrypt(Input::post(__("page"))) : "home");
		$ext = (Input::post(__("extension"), NULL) ? Crypt::decrypt(Input::post(__("extension"))) : NULL);

		$id = (Input::post("id", NULL) ? Input::post("id") : NULL);
		$name = (Input::post("name", NULL) ? Input::post("name") : NULL);
		$value = (Input::post("value", NULL) ? Input::post("value") : NULL);

		switch($action){
			/*overall*/
			case "language": API_Define::setLangId(); break;/*Change language*/
			case "login": $link = (User::Login($name,$value) ? "home" : "login"); break;/*Login user*/
			/*settings*/
			case "help":/*Change "Help" text*/
			case "home":/*Change "Home" text*/
			case "rules": API_Define::setText($action,Input::post(__("editor")),$ext); $link = "settings/edit/".$action."/".$ext; break;/*Change "Rules" text*/
			case "values": API_Define::setValue(substr($name,0,-1),$value,$id); $link = "settings/redefine/".$action."/".$ext; break;/*Change System texts*/
			case "menu": Menu::saveMenu(); $link = "settings/redefine/menus/role/".$ext; break;/*Redefine menus*/
			/*manage*/
			case "imageupload": User::setImageUpload($ext); $link = "manage/users/image/".$ext; break;/*Upload user image*/
			case "password": $result = User::changeUserPassword($ext,sha1($id),sha1($name),sha1($value)); $link = "manage/users".$result; break;/*Change user password*/
			case "adduser": User::saveUser(); $link = "manage/users"; break;/*Add new user*/
			case "edituser": User::saveUser($ext); $link = "manage/users/user/".$ext; break;/*Save user edit*/
			case "deleteuser": User::deleteUser($ext); $link = "manage/users"; break;/*Delete user*/
		}

		return $link;
	}

	public static function MainPage(){
		global $main;
		global $timer;
		global $page;

		$main->assign('base_url', Functions::Host());
		$main->assign('theme', THEME);
		$main->assign('getpage', self::$getpage);
		$main->assign('selfpage', Define::Translate(self::$selfpage));
		$timer = Functions::Timer($timer);
		$main->assign('timer', $timer);
		$main->assign('content', $page->draw(SetPage::$getpage, $return_string=true));
		$main->draw('main');
	}

	public static function Page($get = 'home'){
		global $page;

		switch($get){
			/*help*/
			case 'help/help': self::setMainText('help'); break;
			case 'help/about_us': self::setMainText('about_us'); break;//--
			case 'help/bugs': self::setMainText('bugs'); break;//--
			case 'help/suggestions': self::setMainText('suggestions'); break;//--
			case 'help/rules': self::setMainText('rules'); break;
			case 'help/contact_us': self::setMainText('contact_us'); break;//--
			/*manage*/
			case 'manage': self::setInnerMenus('manage'); break;//setInnerMenus
			case 'manage/users': self::manageUsers(); break;//setManageUsers
			case 'manage/users/user': self::userEdit(); break;//setManageUsersEdit
			case 'manage/users/image': self::manageUsersImage(); break;//setManagaeUsersImage
			case 'manage/users/password': self::manageUsersPassword(); break;//setManageUsersPassword
			case 'manage/groups': self::manageGroups(); break;//setManageGroups
			case 'manage/roles': self::manageRoles(); break;//setManageRoles
			case 'manage/invites': self::manageInvites(); break;//setManageInvites
			case 'manage/invites/invite': self::setManageInvitesInvite(); break;
			case 'manage/levels': self::setManageLevels(); break;
			case 'manage/levels/level': self::setManageLevelsLevel(); break; 
			/*settings*/
			case 'settings': self::setInnerMenus('settings'); break;//setInnerMenus
			case 'settings/edit/help': self::settingsEdit('help'); break;
			case 'settings/edit/home': self::settingsEdit('home'); break;
			case 'settings/edit/rules': self::settingsEdit('rules'); break;
			case 'settings/redefine/values': self::settingsRedefineValues(); break;
			case 'settings/redefine/menus': self::settingsRedefineMenus(); break;
			case 'settings/redefine/menus/role': self::settingsRedefineMenusRole(); break;
			/*home*/
			case 'home': self::Home(); break;
			/*user*/
			case 'login': self::loginPage(); break;
			/*profile*/
			case 'profile': 
			case 'manage/my_profile': self::myProfile(); break;
		}
	}

	public static function DefaultsPre(){
		self::$getpage = rtrim(Input::get("opt"),'/');
		self::$getpageid = explode("/", self::$getpage);
		self::$getpageid = end(self::$getpageid);
		self::$fullpage = self::$getpage;
		if(is_numeric(self::$getpageid) || self::$getpageid == 'n'){
			self::$getpage = substr(self::$fullpage,0,strlen(self::$fullpage)-strlen(self::$getpageid)-1);
		} else {
			self::$getpageid = 0;
		}
		$self = explode("/",self::$getpage);
		self::$selfpage = $self[EXPLODE];
	}

	public static function DefaultsPost(){
		global $page;

		self::$menu = Menu::buildMenu();
		$page->assign('menu', self::$menu);
		$page->assign('actionu', (LEVEL == 1 ? 'login' : 'logout'));
		$page->assign('actionpic', (LEVEL == 1 ? 'login' : 'logout'));
		$page->assign('getpage', SetPage::$fullpage);
		$page->assign('getpage_s', __(SetPage::$fullpage));
		$page->assign('language_s', __('language'));
		$page->assign('action_name',__('action'));
		$page->assign('page_name',__('page'));
		$page->assign('ext_name',__('extension'));

		foreach(Define::getLoopValues() AS $loop){
			$page->assign($loop['name'],$loop['value']);
		}

		$page->assign('theme', THEME);
		$page->assign('user_name',User::loggedUserName());
	}

	private static function Home(){
		global $page;

		$page->assign("random_text",nl2br(Define::getText('home')));
	}

	private static function setMainText($level){
		global $page;
		self::setInnerMenus('help');

		$page->assign('help_text',nl2br(API_Define::getText($level)));
	}

	private static function settingsEdit($level){
		global $page;
		self::setInnerMenus('settings');

		$page->assign('action_s',__($level));
		$page->assign('ext_s',__(self::$getpageid));
		$page->assign('editor_text',API_Define::getText($level,self::$getpageid));
		$page->assign('editor_name',__('editor'));
	}

	private static function settingsRedefineValues(){
		global $page;
		self::setInnerMenus('settings');

		$page->assign('action_s',__('values'));
		$page->assign('ext_s',__(self::$getpageid));
	}

	private static function setInnerMenus($name){
		global $page;

		$page->assign($name,API_Define::getDefines($name));
		$page->assign('datavalues', API_Define::getValues(self::$getpageid));
	}

	private static function loginPage(){
		global $page;

		$page->assign('action_s',__('login'));
	}

	private static function manageUsers(){
		global $page;
		self::setInnerMenus('manage');

		$page->assign('user', User::loggedUserId());
		$page->assign('users', API_Manage::getAllUsers(User::loggedUserId()));
		$page->assign('roles', API_Manage::getRolesList());
	}

	private static function manageUsersImage(){
		global $page;
		self::setInnerMenus('manage');

		$image = API_User::getUserImage(self::$getpageid);

		$page->assign('image',(file_exists($image) ? $image : NULL));
		$page->assign('action_s',__('imageupload'));
		$page->assign('ext_s',__(self::$getpageid));
		$page->assign('ext', self::$getpageid);
	}

	private static function manageUsersPassword(){
		global $page;
		self::setInnerMenus('manage');

		$page->assign('action_s',__('password'));
		$page->assign('ext_s',__(self::$getpageid));
		$page->assign('ext',self::$getpageid);
	}

	private static function settingsRedefineMenus(){
		global $page;
		self::setInnerMenus('settings');
		
		$page->assign('roles', API_Role::getRolesNames());
	}

	private static function settingsRedefineMenusRole(){
		global $page;
		self::setInnerMenus('settings');

		$role = API_Role::getRoleById(self::$getpageid);
		if(!self::$getpageid || !$role){
			header('Location: /settings/redefine/menus');
			die;
		}
		$menus = API_Menu::getMenusByRoleId(self::$getpageid);
		$ids = "";
		foreach($menus AS $key => $value){
			$ids .= $value['id'].",";
		}
		$page->assign('menus', $menus);
		$page->assign('ids',__(substr($ids,0,-1)));
		$page->assign('role_name',API_Define::Translate($role['name']));
		$page->assign('action_s',__('menu'));
		$page->assign('ext_s',__(self::$getpageid));
	}

	private static function manageGroups(){
		global $page;
		self::setInnerMenus('manage');
	}

	private static function userEdit(){
		global $page;
		self::setInnerMenus('manage');

		if(!(API_User::userExists(self::$getpageid)) && self::$getpageid != 'n'){
			header('Location: /manage/users');
		}
		$id = (self::$getpageid ? self::$getpageid : 'n');

		$cols = array('id','username','name','surname','email','phone','blocked');
		$cols2 = array('id','username','name','surname','email','phone','blocked', 'password1', 'password2', 'role');
		$user = API_User::getUserData($id);

		if($id != 'n'){
			$role = API_Role::checkUserRole($id);
			$page->assign('role_id', $role['role_id']);
			$page->assign('action_s',__('edituser'));	
			$page->assign('isid',TRUE);
			$page->assign('action2_s', __('deleteuser'));
		} else {
			$page->assign('action_s',__('adduser'));
			$page->assign('isid',FALSE);
		}
		foreach($cols AS $col){
			$page->assign($col.'_val', $user[$col]);
		}
		foreach($cols2 AS $col){
			$page->assign($col.'_name', __($col));
		}
		$page->assign('blocked_cry', __('0'));
		$page->assign('ext_s',__($id));
		$page->assign('id',User::loggedUserId());
		$page->assign('roles', API_Role::getRolesNames());
		$page->assign('ext', self::$getpageid);
	}
	private static function manageRoles(){
		global $page;
		self::setInnerMenus('manage');
		$page->assign('roles', API_Role::getRolesNames());
	}

	private static function manageInvites(){
		global $page;
		self::setInnerMenus('manage');
	}

	private static function setManageInvitesInvite(){
		global $page;
		self::setInnerMenus('manage');
	}

	private static function myProfile(){
		global $page;
		if(IS_MANAGER){
			self::setInnerMenus('manage');
		}
		$page->assign('is_manager',IS_MANAGER);
	}

	private static function setManageLevels(){
		global $page;
		self::setInnerMenus('manage');
	}

	private static function setManageLevelsLevel(){
		global $page;
		self::setInnerMenus('manage');
	}

}