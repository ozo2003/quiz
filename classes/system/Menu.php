<?php

class Menu {

	public static function rebuildMenuValues(){
		for($i=1;$i<=2;$i++){
			$roles = API_Role::getRolesIds();
			foreach($roles AS $r){
				$data = "<?php\n\n\$menu = array(\n";
				$data2 = "<?php\n\n\$level = array(\n";
				foreach(API_Menu::buildMenu($i,$r['id']) AS $key => $menu){
					$data .= "    array('lower'=>'".$menu['lower']."','name'=>'".str_replace("'","&#039;",$menu['name'])."'),\n";
					$data2 .= "    '".$menu['lower']."',\n";
				}
				$data = substr($data,0,-2);
				$data2 = substr($data2,0,-2);
				$data .= "\n);\n";
				$data2 .= "\n);\n";
				file_put_contents(DEFINEFOLDER.$i."/menu/".$r['id'].".php",$data);
				if($i==1){
					file_put_contents(DEFINEFOLDER."/level/".$r['id'].".php",$data2);
				}
			}
		}		
	}

	public static function buildMenu($lang_id = LANG, $role_id = ROLE){
		$filename = DEFINEFOLDER.$lang_id."/menu/".$role_id.".php";
		if(is_file($filename)){
			$menus = preg_replace('/(^<\?(php)?(\s|\n)+|(\? >))/i', '', file_get_contents($filename));
			eval($menus);

			return $menu;
		}
		return API_Menu::buildMenu($lang_id,$role_id);
	}

	public static function saveMenu(){
		$ids = (Input::post('ids',NULL) ? Crypt::decrypt(Input::post('ids')) : NULL);
		$ids = explode(',',$ids);
		$last = 0;
		foreach($ids AS $id){
			$name = Input::post('name_'.$id);
			$visible = Input::post('visible_'.$id, NULL, 1);
			$order = ($visible ? Input::post('order_'.$id) : 0);
			$order = (!$order && $visible ? $last+1 : $order);
			$last = $order;
			$a = Input::post('english_'.$id);
			$b = Input::post('latvian_'.$id);
			$a = ($a ? $a : $b);
			$b = ($b ? $b : $a);
			if($a && $b){
				API_Menu::saveMenu($id,$name,$order,$visible,$a,$b);
			}
		}
		Menu::rebuildMenuValues();
	}

	public static function isPageAllowed($page = 'home', $lang_id = LANG, $role_id = ROLE){
		$filename = DEFINEFOLDER.$lang_id."/level/".$role_id.".php";
		if(is_file($filename)){
			$levels = preg_replace('/(^<\?(php)?(\s|\n)+|(\? >))/i', '', file_get_contents($filename));
			eval($levels);
			if(in_array($page, $level)){
				return TRUE;
			}
			return FALSE;
		}
		return TRUE;
	}

}