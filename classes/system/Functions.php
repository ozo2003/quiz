<?php

class Functions {

	public static function Timer($start = 0) {
		if(!$start) {
			return microtime(true); 
		} else {
			$finish = microtime(true);  
			return number_format($finish - $start, 4);
		}
	}

	public static function Host() {
		$host = $_SERVER['HTTP_HOST'];
		if (substr($host, 0, 7) != 'http://') {
			$host = 'http://' . $host;
		}
		$host .= ROOT;
		return $host;
	}

	public static function fillSession($array, $level){
		foreach($array AS $key => $value){
			$_SESSION[$level][$key] = $value;
		}
	}

	public static function unId($len = 5) {
		$str = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890';
		return substr(str_shuffle($str), 0, $len);
	}

	public static function checkEmail($email) {
		$regex = "/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/";
		return preg_match($regex, $email) ? 1 : 0;
	}

	public static function getRatio($w, $h){
		return $w / $h;
	}

}