<?php

class Input {

	public static function get($key, $default = NULL, $bool = 0){
		if(!$bool){
			$val = ((empty($_GET) || empty($_GET[$key])) ? $default : $_GET[$key]);
		} else {
			$val = ((!empty($_GET)) && isset($_GET[$key]) ? 1 : 0);
		}
		return $val;
	}

	public static function post($key, $default = NULL, $bool = 0){
		if(!$bool){
			$val = ((empty($_POST) || empty($_POST[$key])) ? $default : $_POST[$key]);
		} else {
			$val = ((!empty($_POST)) && isset($_POST[$key]) ? 1 : 0);
		}
		return $val;
	}

	public static function unid($len = 5) {
		$str = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890';
		return substr(str_shuffle($str), 0, $len);
	}

	public static function error_msg($fun, $error) {
		$data = $fun . ' : ' . $error;
		die($data);
	}

	public static function returnError($text){
		return array('error' => $text);
	}

	public static function gets(){
		return (empty($_GET) ? NULL : $_GET);
	}

	public static function posts(){
		return (empty($_POST) ? NULL : $_POST);
	}

	public static function file($key, $default = NULL){
		$val = ((empty($_FILES) || empty($_FILES[$key])) ? $default : $_FILES[$key]);
		return $val;
	}

}