<?php

class APICommands {

	public static function sendRequest($com, $class, $data = NULL){
		$url = API_URL;
		$command['command'] = $com;
		$command['class'] = $class;
		if($data){
			$command['data'] = $data;
		}
		$request = json_encode($command);
		$response = NULL;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, array('request' => $request));
        $response = curl_exec($ch);
        $response = json_decode($response,true);
        return $response['data'];
	}
	
}