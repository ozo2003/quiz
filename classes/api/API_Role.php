<?php

class API_Role extends APICommands {

	const className = "Role";

	public static function getRolesIds(){
		$result = self::sendRequest('getRolesIds',self::className);

		return $result;
	}

	public static function getRoleById($role_id){
		$data = array(
			'role_id' => $role_id
		);
		$result = self::sendRequest('getRoleById',self::className,$data);

		return $result[0];
	}

	public static function getRolesNames($user_id = NULL) {
		if(!$user_id){
			$user_id = User::loggedUserId();
		}
		$data = array(
			'user_id' => $user_id,
			'role_id' => ROLE
		);
		$result = self::sendRequest('getRolesNames',self::className,$data);
		for($i=0;$i<count($result);$i++){
			$result[$i]['name'] = API_Define::Translate($result[$i]['name']);
		}

		return $result;
	}

	public static function checkUserRole($user_id){
		$data = array(
			'user_id' => $user_id
		);
		$result = self::sendRequest('checkUserRole',self::className,$data);

		return $result;
	}

}