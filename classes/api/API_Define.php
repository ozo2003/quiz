<?php

class API_Define extends APICommands {

	const className = 'Define';

	public static function getLangId(){
		if(isset($_SESSION['lang_id'])){
			return $_SESSION['lang_id'];
		} else {
			$data = array(
				'ip' => IP
			);
			$result = self::sendRequest('getLangId',self::className,$data);
			$lang_id = $result[0]['language_id'];
			$_SESSION['lang_id'] = $lang_id;

			return $lang_id;
		}
	}

	public static function getLangIdByName($name){
		$data = array(
			'name' => $name
		);
		return self::sendRequest('getLangIdByName',self::className,$data);
	}

	public static function setLangId(){
		$lang_id = (LANG == 1 ? 2 : 1);
		$_SESSION['lang_id'] = $lang_id;
		$data = array(
			'ip' => IP, 
			'lang_id' => $lang_id
		);

		return self::sendRequest('setLangId',self::className,$data);
	}

	public static function setText($type,$editor,$ext){
		$data = array(
			'name' => $type,
			'value' => $editor,
			'lang_id' => $ext
		);
	
		$result = self::sendRequest('setText',self::className,$data);
		Define::rebuildHelpValues();

		return $result;
	}

	public static function getText($name,$ext = LANG){
		$data = array(
			'name' => $name,
			'lang_id' => $ext
		);
		$result = self::sendRequest('getText',self::className,$data);

		return $result[0]['value'];
	}

	public static function getLoopValues($lang_id = LANG){
		$data = array(
			'lang_id' => $lang_id
		);
		$result = self::sendRequest('getLoopValues',self::className,$data);

		return $result;
	}

	public static function getDefines($name){
		$data = array(
			'name' => $name,
			'lang_id' => LANG
		);
		$result = self::sendRequest('getDefines',self::className,$data);

		return $result;
	}

	public static function getValues($lang_id = LANG){
		$data = array(
			'lang_id' => $lang_id
		);
		$result = self::sendRequest('getValues',self::className,$data);

		return $result;
	}

	public static function setValue($name,$value,$id){
		$data = array(
			'value' => $value,
			'name' => $name,
			'id' => $id
		);
		$result = self::sendRequest('setValue',self::className,$data);
		Define::rebuildLoopValues();
		
		return $result;
	}

	public static function Translate($name){
		$data = array(
			'name' => $name,
			'lang_id' => LANG
		);
		$result = self::sendRequest('Translate',self::className,$data);

		return $result[0]['value'];
	}

	public static function getTextNames(){
		return self::sendRequest('getTextNames',self::className);
	}

	public static function helpIdByName($name,$lang_id = LANG){
		$data = array(
			'name' => $name,
			'lang_id' => $lang_id
		);
		$result = self::sendRequest('helpIdByName',self::className,$data);

		return $result[0]['id'];
	}

}