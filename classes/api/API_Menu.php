<?php

class API_Menu extends APICommands {

	const className = 'Menu';

	public static function buildMenu($lang_id = LANG, $role_id = ROLE){
		$data = array(
			'lang_id' => $lang_id,
			'role_id' => $role_id
		);
		$result = self::sendRequest('buildMenu',self::className,$data);
		
		return $result;
	}

	public static function getMenusByRoleId($role_id){
		$data = array(
			'role_id' => $role_id
		);
		$result = self::sendRequest('getMenusByRoleId',self::className,$data);
		
		return $result;
	}

	public static function saveMenu($menu_id,$name,$order = 0,$visible = 0,$a = "",$b = ""){
		$data = array(
			'menu_id' => $menu_id,
			'name' => $name,
			'order' => $order,
			'visible' => $visible,
			'a' => $a,
			'b' => $b
		);
		self::sendRequest('saveMenu',self::className,$data);
	}

}