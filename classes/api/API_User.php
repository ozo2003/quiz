<?php

class API_User extends APICommands {

	const className = "User";

	public static function checkUser($user,$pass){
		$data = array(
			'user' => $user,
			'password' => $pass
		);
		$result = self::sendRequest('checkUser',self::className,$data);

		return $result[0];
	}

	public static function setAdminIp(){
		$data = array(
			'ip' => IP
		);
		$result = self::sendRequest('setAdminIp',self::className,$data);

		return $result;
	}

	public static function getUserImage($user_id){
		$data = array(
			'user_id' => $user_id
		);
		$result = self::sendRequest('getUserImage',self::className,$data);
		
		return $result[0]['image'];
	}

	public static function setUserImage($user_id,$name){
		$data = array(
			'user_id' => $user_id,
			'name' => $name
		);
		self::sendRequest('setUserImage',self::className,$data);
	}

	public static function changeUserPassword($user_id,$password,$newpassword){
		$data = array(
			'user_id' => $user_id,
			'password' => $password,
			'newpassword' => $newpassword
		);
		self::sendRequest('changeUserPassword',self::className,$data);
	}

	public static function getUserData($user_id){
		if(!$user_id){
			$result = array(
				'id' => 0,
				'username' => '',
				'name' => '',
				'surname' => '',
				'email' => '',
				'phone' => '',
				'blocked' => ''
			);
			return $result;
		}
		$data = array(
			'user_id' => $user_id
		);
		$result = self::sendRequest('getUserData',self::className,$data);

		return $result[0];
	}

	public static function userExists($user_id){
		$data = array(
			'user_id' => $user_id
		);
		$result = self::sendRequest('userExists',self::className,$data);

		return $result;
	}

	public static function revokeUserImage($user_id){
		$data = array(
			'user_id' => $user_id
		);
		self::sendRequest('revokeUserImage',self::className,$data);
	}

	public static function addUser($data,$role_id){
		$result = self::sendRequest('addUser',self::className,$data);
	}

	public static function editUser($data,$role_id){
		$result = self::sendRequest('editUser',self::className,$data);
	}

	public static function deleteUser($user_id){
		$data = array(
			'user_id' => $user_id
		);
		$result = self::sendRequest('deleteUser',self::className,$data);
	}

}