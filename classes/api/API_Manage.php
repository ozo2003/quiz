<?php

class API_Manage extends APICommands {

	const className = 'Manage';

	public static function getAllUsers($user_id){
		$data = array(
			'lang_id' => LANG,
			'user_id' => $user_id,
			'role_id' => ROLE
		);
		$result = self::sendRequest('getAllUsers',self::className,$data);

		return $result;
	}

	public static function getRoles(){
		$result = self::sendRequest('getRoles',self::className);
		
		return $result;
	}

	public static function getRolesList(){
		$data = array(
			'lang_id' => LANG
		);
		$result = self::sendRequest('getRolesList',self::className,$data);

		return $result;
	}

}