<?php

require 'Database.php';

class Extended extends Database {

	public function change_language($lang_id) {
		$this->fill_session(array('lang_id' => $lang_id));
		$this->result = $this->PDO->prepare("
			UPDATE
				ips
			SET
				language_id = ?
			WHERE
				ip = ?
		");
		try {
			$this->result->execute(array($lang_id, $this->get_ip()));
		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

	public function fill_session($data) {
		$this->keys = array_keys($data);
		for ($i = 0; $i < count($this->keys); $i++) {
			$_SESSION[$this->keys[$i]] = $data[$this->keys[$i]];
		}
	}

	public function one_key ($array, $key) {
		$this->data2 = NULL;
		$this->data2 = $array[$key];
		return $this->data2;
	}

	public function loop_values() {
		$this->result = $this->PDO->prepare("
			SELECT
				name,
				value
			FROM
				defines
			WHERE
				`loop` = 1
			AND
				language_id = ?
		");
		try {
			$this->result->execute(array($this->lang_id));
		} catch (Exception $e) {
			die($e->getMessage());
		}
		$this->data = NULL;
		$i = 0;
		while($this->row = $this->result->fetch(PDO::FETCH_ASSOC)) {
			$this->data[$i] = $this->row;
			$i++;
		}

		return $this->data;
	}

	public function setThemeIP($name) {
		$this->result = $this->PDO->prepare("UPDATE ips SET theme = ? WHERE ip = ?");
		try {
			$this->result->execute(array($name,$this->get_ip()));
		} catch (Exception $e) {
			$this->error_msg(__FUNCTION__,$e->getMessage());
		}
	}

	public function getThemeIP() {
		$this->result = $this->PDO->prepare("SELECT theme FROM ips WHERE ip = ?");
		try {
			$this->result->execute(array($this->get_ip()));
		} catch (Exception $e) {
			$this->error_msg(__FUNCTION__,$e->getMessage());
		}
	}

	public function setTheme($name) {
		$this->result = $this->PDO->prepare("

		");
	}

	public function getTheme() {
		
	}

}
