<?php

require 'Extended.php';

class Login extends Extended {

	public function login_user($user, $pass) {
		session_destroy();
		session_start();
		$this->user_id = $this->check_data($user, $pass);
		if($this->user_id) {
			$this->data = $this->check_role($this->user_id);
			$this->setRole();
			if($_SESSION['role_id'] == 4) {
				$this->admin_ip();
			}
		}
	}

	public function check_data($user, $pass) {
		$pass = sha1($pass);
		$this->result = $this->PDO->prepare("
			SELECT
				u.id AS user_id,
				u.username,
				IFNULL(u.name,'') AS name,
				IFNULL(u.surname,'') AS surname,
				IFNULL(u.email,'') AS email,
				IFNULL(i.language_id,1) AS lang_id
			FROM
				users u
			LEFT JOIN
				ips i ON i.user_id = u.id
			WHERE
				(u.username = ? OR u.email = ? OR u.phone = ?)
			AND
				u.password = ?
		");
		try {
			$this->result->execute(array($user, $user, $user, $pass));
		} catch (Exception $e) {
			die($e->getMessage());
		}
		$this->count = $this->result->rowCount();
		if ($this->count) {
			$this->row = $this->result->fetch(PDO::FETCH_ASSOC);
			$this->fill_session($this->row);
			return $this->row['user_id'];
		}
		return 0;
	}

	public function check_role($user_id) {
		$this->result = $this->PDO->prepare("
			SELECT
				IFNULL(role_id, 1) AS role_id
			FROM
				users_roles
			WHERE
				user_id = ?
		");
		try {
			$this->result->execute(array($user_id));
		} catch (Exception $e) {
			die($e->getMessage());
		}
		$this->count = $this->result->rowCount();
		if ($this->count) {
			$this->row = $this->result->fetch(PDO::FETCH_ASSOC);
			$this->fill_session($this->row);
			return;
		}
		$this->fill_session(array('role_id'=>1));
	}

	public function admin_ip() {
		$this->result = $this->PDO->prepare("
			UPDATE
				settings
			SET
				value = ?
			WHERE
				name = 'adminip'
		");
		try {
			$this->result->execute(array($this->get_ip()));
		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

}

?>
