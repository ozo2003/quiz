<?php

require 'Authenticate.php';

class Database extends Authenticate {

	public $PDO;
	public $result;
	public $result2;
	public $row;
	public $row2;
	public $host;
	public $role_id;
	public $data;
	public $data2;
	public $count;
	public $keys;
	public $lang_id = 1;
	public $user_id;
	public $up_dir;
	public $sql;

	public function __construct() {
		//error_reporting(0);
		date_default_timezone_set('Europe/Riga');
		ini_set('session.gc_maxlifetime', '3600');
		$this->connect();
		ini_set('display_errors', 1);
		error_reporting(E_ALL | E_STRICT);
		$this->host_check();
		$this->setRole();
		$this->set_lang_id();
		$this->up_dir = $_SERVER['DOCUMENT_ROOT'] . $this->root . 'upload/';
	}

	public function connect() {
		try {
			$this->PDO = new PDO("mysql:host=".$this->db_host.";dbname=$this->db_name", $this->db_user, $this->db_pass, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
			$this->PDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		} catch(Exception $e) {
			$this->error_msg(__FUNCTION__, $e->getMessage());
		}
	}

	public function build_menu() {
		$this->result = $this->PDO->prepare("
			SELECT
				m.`".$this->lang_id."` AS name,
				m.name AS lower,
				CASE WHEN m.`order` = 0 THEN 999 ELSE m.`order` END AS morder
			FROM
				menus m
			WHERE
				m.visible = 1
			AND
				m.role_id = ?
			ORDER BY
				morder
			LIMIT
				6
		");
		try {
			$this->result->execute(array($this->role_id));
		} catch (Exception $e) {
			$this->error_msg(__FUNCTION__, $e->getMessage());
		}
		$this->data = NULL;
		$i = 0;
		while ($this->row = $this->result->fetch(PDO::FETCH_ASSOC)) {
			$d = $this->row;
			$this->data[$i] = $d;
			$i++;
		}
		return $this->data;
	}

	public function host_check() {
		$this->host = $_SERVER['HTTP_HOST'];
		if (substr($this->host, 0, 7) != 'http://') {
			$this->host = 'http://' . $this->host;
		}
		$this->host .= $this->root;
	}

	public function user_data() {
		return $_SESSION;
	}

	public function user_name($data) {
		$this->data = NULL;
		if (isset($data['name'])) {
			$this->data = $data['name']." ".$data['surname'];
		} else {
			$this->data = $this->translate('guest');
		}
		return $this->data;
	}

	public function setRole() {
		if (isset($_SESSION['role_id'])) {
			$this->role_id = $_SESSION['role_id'];
		} else {
			$this->role_id = 1;
		}
	}

	public function get_ip() {
		return $_SERVER['REMOTE_ADDR'];
	}

	public function set_lang_id() {
		if (isset($_SESSION['lang_id'])) {
			$this->lang_id = $_SESSION['lang_id'];
		} else {
			$this->result = $this->PDO->prepare("
				SELECT
					language_id
				FROM
					ips
				WHERE
					ip = ?
			");
			try {
				$this->result->execute(array($this->get_ip()));
			} catch (Exception $e) {
				$this->error_msg(__FUNCTION__, $e->getMessage());
			}
			$this->row = $this->result->fetch(PDO::FETCH_ASSOC);
			$this->count = $this->result->rowCount();
			if($this->count) {
				$this->lang_id = $this->row['language_id'];
			}
		}
	}

	public function translate($name = NULL) {
		$this->result2 = $this->PDO->prepare("
			SELECT
				name,
				value
			FROM
				defines
			WHERE
				language_id = ?
		");
		try {
			$this->result2->execute(array($this->lang_id));
		} catch (Exception $e) {
			$this->error_msg(__FUNCTION__, $e->getMessage());
		}
		$this->data2 = NULL;
		while ($this->row2 = $this->result2->fetch(PDO::FETCH_ASSOC)) {
			$this->data2[$this->row2['name']] = $this->row2['value'];
		}

		if($name) {
			return $this->data2[$name];
		}
		return $this->data2;
	}

	public function foreign($c = 0) {
		$this->result2 = $this->PDO->prepare("
			SET foreign_key_checks = ?
		");
		try {
			$this->result2->execute(array($c));
		} catch (Exception $e) {
			$this->error_msg(__FUNCTION__, $e->getMessage());
		}
	}

	public function error_msg($fun, $error) {
		$this->data = $fun . ' : ' . $error;
		/*$this->result = $this->PDO->prepare("
			INSERT INTO
				logs
					(function,error)
			VALUES
				(?,?)
		");
		try {
			$this->result->execute(array($fun, $error));
		} catch (Exception $e) {
			$this->error_msg(__FUNCTION__, $e->getMessage());
		}*/
		die($this->data);
	}

	public function unid($len = 5) {
		$str = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890';
		return substr(str_shuffle($str), 0, $len);
	}

}

?>
