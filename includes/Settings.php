<?php

require 'Extended.php';

class Settings extends Extended {

	public function redefine_value($name, $value, $id) {
		$this->result = $this->PDO->prepare("
			UPDATE
				defines
			SET
				value = ?
			WHERE
				name = ?
			AND
				id = ?
		");
		try {
			$this->result->execute(array($value, $name, $id));
		} catch (Exception $e) {
			$this->error_msg(__FUNCTION__, $e->getMessage());
		}
	}

	public function get_defines($type) {
		try {
			$this->result = $this->PDO->prepare("
				SELECT
					d.value as name,
					ds.function as function,
					d.comment
				FROM
					definable_settings ds
				LEFT JOIN
					defines d ON d.name = ds.name
				WHERE
					d.language_id = ?
				AND
					ds.`type` = ?
				ORDER BY
					d.value
			");
			$this->result->execute(array($this->lang_id, $type));
		} catch (Exception $e) {
			$this->error_msg(__FUNCTION__, $e->getMessage());
		}
		$this->data = NULL;
		$i = 0;
		while($this->row = $this->result->fetch(PDO::FETCH_ASSOC)) {
			$this->data[$i] = $this->row;
			$i++;
		}

		return $this->data;
	}

	public function get_values($lang_id) {
		$this->result = $this->PDO->prepare("
			SELECT
				id,
				name,
				value,
				comment
			FROM
				defines
			WHERE
				language_id = ?
			AND
				type_id = 1
			ORDER BY
				name
		");
		try {
			$this->result->execute(array($lang_id));
		} catch (Exception $e) {
			$this->error_msg(__FUNCTION__, $e->getMessage());
		}
		$i = 0;
		$this->data = NULL;
		while ($this->row = $this->result->fetch(PDO::FETCH_ASSOC)) {
			$this->data[$i] = $this->row;
			$i++;
		}

		return $this->data;
	}

	public function get_lang_id($lang) {
		$this->result = $this->PDO->prepare("
			SELECT
				id
			FROM
				languages
			WHERE
				`language` = ?
		");
		try {
			$this->result->execute(array($lang));
		} catch (Exception $e) {
			$this->error_msg(__FUNCTION__, $e->getMessage());
		}
		$this->row = NULL;
		$this->row = $this->result->fetch(PDO::FETCH_ASSOC);

		return $this->row['id'];
	}

	public function get_menus() {
		try {
			$this->result = $this->PDO->query("
				SELECT DISTINCT
					name
				FROM
					menus
			");
		} catch (Exception $e) {
			$this->error_msg(__FUNCTION__, $e->getMessage());
		}
		$i = 0;
		$this->data = NULL;
		while($this->row = $this->result->fetch(PDO::FETCH_ASSOC)) {
			$this->data[$i] = $this->row['name'];
			$i++;
		}

		return $this->data;
	}

	public function get_roles() {
		try {
			$this->result = $this->PDO->query("
				SELECT
					r.*,
					(SELECT value FROM defines d WHERE d.name = r.name AND language_id = 1) AS english,
					(SELECT value FROM defines d WHERE d.name = r.name AND language_id = 2) AS latvian
				FROM
					roles r
				WHERE
					r.`delete` <> 1
				ORDER BY
					r.id
			");
		} catch (Exception $e) {
			$this->error_msg(__FUNCTION__, $e->getMessage());
		}

		$this->data = NULL;
		$i = 0;
		while($this->row = $this->result->fetch(PDO::FETCH_ASSOC)) {
			if($this->row['manager_id'] == $this->one_key($_SESSION, 'user_id') || $this->role_id == 4) {
				$this->data[$i] = $this->row;
				$i++;
			}
		}
		return $this->data;
	}

	public function get_menu_names() {
		$roles = $this->get_roles();
		$this->keys = array_keys($roles);
		$menus = $this->get_menus();
		$translated = NULL;

		for($j = 0; $j < count($roles); $j++) {
			$translated[$j] = $this->translate($roles[$j]['name']);
		}

		$this->result = $this->PDO->prepare("
			SELECT DISTINCT
				m.id,
				m.name,
				m.`order`,
				m.visible,
				CASE WHEN m.`order` = 0 THEN 999 ELSE m.`order` END AS `ord`,
				m.`1` AS english,
				m.`2` AS latvian
			FROM
				menus m
			WHERE
				m.role_id = ?
			ORDER BY
				`ord`
		");
		$this->data = NULL;
		for($i = 0; $i < count($roles); $i++) {
			try {
				$this->result->execute(array($roles[$i]['id']));
			} catch (Exception $e) {
				$this->error_msg(__FUNCTION__, $e->getMessage());
			}
			$j = 0;
			while($this->row = $this->result->fetch(PDO::FETCH_ASSOC)) {
				$this->data[$roles[$i]['name']]['data'][$j] = $this->row;
				$j++;
			}
			$this->data[$roles[$i]['name']]['role_tr'] = $translated[$i];
		}

		return $this->data;
	}

	public function get_menus_id() {
		try {
			$this->result = $this->PDO->query("
				SELECT
					id
				FROM
					menus
			");
		} catch (Exception $e) {
			$this->error_msg(__FUNCTION__, $e->getMessage());
		}
		$i = 0;
		$this->data = NULL;
		while($this->row = $this->result->fetch(PDO::FETCH_ASSOC)) {
			$this->data[$i] = $this->row['id'];
			$i++;
		}

		return $this->data;
	}

	public function get_roles_menus_id($id) {
		$this->result = $this->PDO->prepare("
			SELECT
				id
			FROM
				menus
			WHERE
				role_id = ?
		");
		try {
			$this->result->execute(array($id));
		} catch (Exception $e) {
			$this->error_msg(__FUNCTION__, $e->getMessage());
		}
		$this->data = NULL;
		$i = 0;
		while($this->row = $this->result->fetch(PDO::FETCH_ASSOC)) {
			$this->data[$i] = $this->row['id'];
			$i++;
		}

		return $this->data;
	}

	public function update_menus($id, $data) {
		$data['id'] = $id;
		$this->result = $this->PDO->prepare("
			UPDATE
				menus
			SET
				name = :name,
				`order` = :order,
				visible = :visible,
				`2` = :latvian,
				`1` = :english
			WHERE
				id = :id
		");
		try {
			$this->result->execute($data);
		} catch (Exception $e) {
			$this->error_msg(__FUNCTION__, $e->getMessage());
		}
	}

	public function get_users($id) {
		try {
			$this->result = $this->PDO->query("
				SELECT
					id,
					CONCAT(name, ' ', surname) AS name
				FROM
					users
				ORDER BY
					id
			");
		} catch (Exception $e) {
			$this->error_msg(__FUNCTION__, $e->getMessage());
		}

		$this->data = NULL;
		$i = 0;
		while($this->row = $this->result->fetch(PDO::FETCH_ASSOC)) {
			if($this->row['manager_id'] == $this->one_key($_SESSION, 'user_id') || $this->role_id == 4 || $this->row['manager_id'] == 0) {
				if($this->row['id'] == $id) {
					$this->data = $this->row;
					break;
				} else {
					$this->data[$i] = $this->row;
					$i++;
				}
			}
		}
		return $this->data;
	}

	public function get_managers() {
		try {
			$this->result = $this->PDO->query("
				SELECT
					u.id,
					CONCAT(u.name, ' ', u.surname) AS name
				FROM
					users u
				LEFT JOIN
					users_roles ur ON ur.user_id = u.id
				LEFT JOIN
					roles r ON ur.role_id = r.id
				WHERE
					r.manager = 1
				ORDER BY
					u.id
			");
		} catch (Exception $e) {
			$this->error_msg(__FUNCTION__, $e->getMessage());
		}

		$this->data = NULL;
		$i = 0;
		while($this->row = $this->result->fetch(PDO::FETCH_ASSOC)) {
			$this->data[$i] = $this->row;
			$i++;
		}
		return $this->data;
	}

	public function update_roles($data) {
		$this->update_roles_def($data['name'], $data['english'], $data['latvian']);
		$this->update_roles_tb($data['manager_id'], $data['active'], $data['delete'], $data['manager'], $data['id'], $data['name']);
	}

	public function update_roles_tb($manager_id, $active, $delete, $manager, $id, $name) {
		$this->result = $this->PDO->prepare("
			UPDATE
				roles
			SET
				manager_id = ?,
				active = ?,
				`delete` = ?,
				manager = ?
			WHERE
				id = ?
			AND
				name = ?;
			UPDATE
				roles
			SET
				active = 1,
				`delete` = 0
			WHERE
				id IN (1,2,3,4);
			UPDATE
				roles
			SET
				manager = 1
			WHERE
				id in (3,4)
		");
		try {
			$this->result->execute(func_get_args());
		} catch (Exception $e) {
			$this->error_msg(__FUNCTION__, $e->getMessage());
		}
	}

	public function update_roles_def($name, $english, $latvian) {
		$this->result = $this->PDO->prepare("
			UPDATE
				defines
			SET
				value = ?
			WHERE
				name = ?
			AND
				language_id = ?
		");
		try {
			$this->result->execute(array($latvian, $name, 1));
			$this->result->execute(array($english, $name, 2));
		} catch (Exception $e) {
			$this->error_msg(__FUNCTION__, $e->getMessage());
		}
	}

	public function insert_role($data) {
		$this->insert_role_menus($this->insert_role_tb($data['role'], $data['manager_id'], $data['active'], $data['manager']));
		$this->insert_role_def($data['role'], $data['english'], $data['latvian']);
	}

	public function insert_role_tb($name, $manager_id, $active, $manager) {
		if($this->check_role_name($name)) {
			$this->result = $this->PDO->prepare("
				INSERT INTO
					roles
						(name, manager_id, active, manager)
				VALUES
					(?, ?, ?, ?)
			");
			try {
				$this->result->execute(func_get_args());
			} catch (Exception $e) {
				$this->error_msg(__FUNCTION__, $e->getMessage());
			}
			return $this->PDO->lastInsertId();
		}
	}

	public function insert_role_def($name, $english, $latvian) {
		if($this->check_role_name($name)) {
			$this->result = $this->PDO->prepare("
				INSERT INTO
					defines
						(name, value, language_id, type_id)
				VALUES
					(?, ?, ?, 3)
			");
			try {
				$this->result->execute(array($name, $english, 1));
				$this->result->execute(array($name, $english, 2));
			} catch (Exception $e) {
				$this->error_msg(__FUNCTION__, $e->getMessage());
			}
		}
		if(isset($latvian) && $latvian != '') {
			$this->update_roles_def($name, $english, $latvian);
		}
	}

	public function check_role_name($name) {
		$this->result = $this->PDO->prepare("
			SELECT
				name
			FROM
				defines
			WHERE
				name = ?
		");
		try {
			$this->result->execute(array($name));
		} catch (Exception $e) {
			$this->error_msg(__FUNCTION__, $e->getMessage());
		}
		$this->count = $this->result->rowCount();
		if($this->count) {
			return 0;
		}
		return 1;
	}

	public function insert_role_menus($id) {
		$this->result = $this->PDO->prepare("
			INSERT INTO
				menus
					(name, `order`, visible, `1`, `2`, role_id)
			SELECT
				name,
				`order`,
				visible,
				`1`,
				`2`,
				?
			FROM
				menus
			WHERE
				role_id = 4
		");
		try {
			$this->result->execute(array($id));
		} catch (Exception $e) {
			$this->error_msg(__FUNCTION__, $e->getMessage());
		}
	}

	public function get_users_list() {
		$this->result = $this->PDO->prepare("
			SELECT
				u.id,
				u.username,
				IFNULL(u.name,'') AS name,
				IFNULL(u.surname,'') AS surname,
				IFNULL(u.email,'') AS email,
				IFNULL(u.phone,'') AS phone,
				d.value,
				r.id AS role_id,
				u.blocked,
				r.manager_id,
				u.creator_id
			FROM
				users u
			LEFT JOIN
				users_roles ur ON ur.user_id = u.id
			LEFT JOIN
				roles r ON r.id = ur.role_id
			LEFT JOIN
				defines d ON d.name = r.name
			WHERE
				d.language_id = ?
		");
		try {
			$this->result->execute(array($this->lang_id));
		} catch (Exception $e) {
			$this->error_msg(__FUNCTION__, $e->getMessage());
		}
		$this->data = NULL;
		$i = 0;
		while($this->row = $this->result->fetch(PDO::FETCH_ASSOC)) {
			if(
				$this->row['manager_id'] == $this->one_key($_SESSION, 'user_id')
				|| $this->role_id == 4
				|| $this->row['manager_id'] == 0
				|| $this->row['id'] == $this->one_key($_SESSION, 'user_id')
				|| $this->row['creator_id'] == $this->one_key($_SESSION, 'user_id')
			) {
				$this->data[$i] = $this->row;
				$i++;
			}
		}

		return $this->data;
	}

	public function roles_list() {
		$this->result = $this->PDO->prepare("
			SELECT
				r.id,
				d.value,
				r.manager_id
			FROM
				roles r
			LEFT JOIN
				defines d ON r.name = d.name
			WHERE
				d.language_id = ?
			AND
				r.`delete` = 0
			AND
				r.id <> 1
			ORDER BY
				r.id ASC
		");
		try {
			$this->result->execute(array($this->lang_id));
		} catch (Exception $e) {
			$this->error_msg(__FUNCTION__, $e->getMessage());
		}
		$this->data = NULL;
		$i = 0;
		$error = 0;
		while($this->row = $this->result->fetch(PDO::FETCH_ASSOC)) {
			if(
				in_array($this->role_id, $this->get_managers_ids())
				|| $this->row['manager_id'] == $this->one_key($_SESSION, 'user_id')
				|| in_array($this->row['id'], array($this->role_id, 2))
			) {
				$error = ($this->row['id'] == 4 ? $i : $error);
				$this->data[$i] = $this->row;
				$i++;
			}
		}
		if($this->role_id != 4 && $error != 0)
			unset($this->data[$error]);

		return $this->data;
	}

	public function get_managers_ids() {
		try {
			$this->result2 = $this->PDO->query("
				SELECT
					id
				FROM
					roles
				WHERE
					manager = 1
			");
		} catch (Exception $e) {
			$this->error_msg(__FUNCTION__, $e->getMessage());
		}
		$j = 0;
		$this->data2 = NULL;
		while($this->row2 = $this->result2->fetch(PDO::FETCH_ASSOC)) {
			$this->data2[$j] = $this->row2;
			$j++;
		}

		return $this->data2;
	}

	public function get_current_password($id) {
		$this->result = $this->PDO->prepare("
			SELECT
				password
			FROM
				users
			WHERE
				id = ?
		");
		try {
			$this->result->execute(array($id));
		} catch (Exception $e) {
			$this->error_msg(__FUNCTION__, $e->getMessage());
		}
		$this->data = $this->result->fetch(PDO::FETCH_ASSOC);

		return $this->data['password'];
	}

	public function change_user_password($old, $new1, $new2, $id) {
		return $old != '' || $new1 != '' || $new2 != '' ? ($old == $this->get_current_password($id) ? ($new1 == $new2 ? ($old != $new1 ? ($this->update_password($new1, $id)) : 0) : 0) : 0) : 0;
	}

	public function update_password($pass, $id) {
		$this->result = $this->PDO->prepare("
			UPDATE
				users
			SET
				password = ?
			WHERE
				id = ?
		");
		try {
			$this->result->execute(func_get_args());
		} catch (Exception $e) {
			$this->error_msg(__FUNCTION__, $e->getMessage());
		}

		return 1;
	}

	public function update_user_tb($data) {
		$this->keys = array_keys($data);
		$this->count = count($this->keys);
		for($i = 0; $i < $this->count; $i++) {
			if(in_array($this->keys[$i], array('role', 'id'))) {
				${$this->keys[$i]} = $data[$this->keys[$i]];
				unset($this->keys[$i]);
			}
		}
		for($i = 0; $i < $this->count; $i++){
			if(isset($this->keys[$i])) {
				$this->result = $this->PDO->prepare("
					UPDATE
						users
					SET
						".$this->keys[$i]." = ?
					WHERE
						id = ?
				");
				try {
					$this->result->execute(array($data[$this->keys[$i]], $id));
				} catch (Exception $e) {
					$this->error_msg(__FUNCTION__, $e->getMessage());
				}
			}
		}
		return $this->update_user_role($role, $id);
	}

	public function update_user_role($role, $id) {
		$this->result2 = $this->PDO->prepare("
			UPDATE
				users_roles
			SET
				role_id = ?
			WHERE
				user_id = ?
		");
		try {
			$this->PDO->query("SET foreign_key_checks = 0");
			$this->result2->execute(array($role, $id));
			$this->PDO->query("SET foreign_key_checks = 1");
		} catch (Exception $e) {
			$this->error_msg(__FUNCTION__, $e->getMessage());
		}

		return 1;
	}

	public function check_email($email) {
		$regex = "/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/";
		return preg_match($regex, $email) ? 1 : 0;
	}

	public function add_user($data) {
		$this->keys = array_keys($data);
		for ($i = 0; $i < count($this->keys); $i++) {
			${$this->keys[$i]} = $data[$this->keys[$i]];
		}

		$error = ($password1 != $password2 || strlen($password1) < 6 || $this->check_new_user($username) ? 1 : 0);
		if(!$error) {
			$this->result = $this->PDO->prepare("
				INSERT INTO
					users
						(username,password,name,surname,email,phone,creator_id)
				VALUES
					(?,?,?,?,?,?,?)
			");
			try {
				$this->result->execute(array($username, sha1($password1), $name, $surname, $email, $phone, $this->one_key($_SESSION, 'user_id')));
			} catch (Exception $e) {
				$this->error_msg(__FUNCTION__, $e->getMessage());
			}
			$id = $this->PDO->lastInsertId();
			$this->insert_user_role($role, $id);
			return 1;
		}
		return 0;
	}

	public function check_new_user($username) {
		$this->result = $this->PDO->prepare("
			SELECT
				id
			FROM
				users
			WHERE
				username = ?
		");
		try {
			$this->result->execute(array($username));
		} catch (Exception $e) {
			$this->error_msg(__FUNCTION__, $e->getMessage());
		}
		$this->count = $this->result->rowCount();
		if($this->count) {
			return 1;
		}
		return 0;
	}

	public function insert_user_role($role, $id) {
		$this->result = $this->PDO->prepare("
			INSERT INTO
				users_roles
					(role_id, user_id)
			VALUES
				(?,?)
		");
		try {
			$this->result->execute(func_get_args());
		} catch (Exception $e) {
			$this->error_msg(__FUNCTION__, $e->getMessage());
		}
	}

	public function user_delete($id) {
		$this->result = $this->PDO->prepare("
			DELETE FROM
				users
			WHERE
				id = ?
			AND
				id NOT IN (1,2)
		");
		try {
			$this->result->execute(array($id));
		} catch (Exception $e) {
			$this->error_msg(__FUNCTION__, $e->getMessage());
		}
	}

	public function get_groups() {
		try {
			$this->result = $this->PDO->query("
				SELECT
					g.id,
					g.name,
					COUNT(ug.user_id) AS counter,
					CONCAT(u.name,' ',u.surname) AS manager,
					IFNULL(ur.role_id,4) AS manager_role
				FROM
					groups g
				LEFT JOIN
					users_groups ug ON ug.group_id = g.id
				LEFT JOIN
					managers_groups mg ON mg.group_id = g.id
				LEFT JOIN
					users_roles ur ON ur.user_id = mg.user_id
				LEFT JOIN
					users u ON u.id = mg.user_id
				GROUP BY
					g.id
			");
		} catch (Exception $e) {
			$this->error_msg(__FUNCTION__, $e->getMessage());
		}
		$i = 0;
		$this->data = NULL;
		while($this->row = $this->result->fetch(PDO::FETCH_ASSOC)) {
			if(
				in_array($this->row['manager'], array($this->one_key($_SESSION, 'user_id'), 0))
				|| $this->row['manager_role'] == $this->role_id
			) {
				$this->data[$i] = $this->row;
				$i++;
			}
		}

		return $this->data;
	}

	public function get_group_members($id) {
		$this->result = $this->PDO->prepare("
			SELECT
				u.id,
				CONCAT(u.name,' ',u.surname) AS name,
				u.username,
				d.value AS role
			FROM
				users u
			LEFT JOIN
				users_groups ug ON ug.user_id = u.id
			LEFT JOIN
				users_roles ur ON ur.user_id = u.id
			LEFT JOIN
				roles r ON r.id = ur.role_id
			LEFT JOIN
				defines d ON d.name = r.name AND d.language_id = ?
			WHERE
				ug.group_id = ?
		");
		try {
			$this->result->execute(array($this->lang_id, $id));
		} catch (Exception $e) {
			$this->error_msg(__FUNCTION__, $e->getMessage());
		}
		$i = 0;
		$this->data = NULL;
		while($this->row = $this->result->fetch(PDO::FETCH_ASSOC)) {
			$this->data[$i] = $this->row;
			$i++;
		}

		return $this->data;
	}

	public function users_left($id) {
		$users = $this->get_users_list();
		$used = $this->get_group_members($id);
		$data1 = NULL;
		$data2 = NULL;
		for($i = 0; $i < count($used); $i++) {
			$data2[$i] = $used[$i]['id'];
		}
		$a = 0;
		for($i = 0; $i < count($users); $i++) {
			if(!in_array($users[$i]['id'], $data2) || !is_array($data2)) {
				$data1[$a] = $users[$i]['id'];
				$a++;
			}
		}
		$this->data = NULL;
		if(count($data1)) {
			$data = implode(',', $data1);
			//$d = preg_replace("/[^,]/", "?", $data);
			$this->result = $this->PDO->prepare("
				SELECT
					u.id,
					CONCAT(u.name,' ',u.surname) AS name,
					u.username
				FROM
					users u
				WHERE
					id IN (".$data.")
			");
			try {
				$this->result->execute(/*array($data)*/);
			} catch (Exception $e) {
				$this->error_msg(__FUNCTION__, $e->getMessage());
			}
			$i = 0;
			while($this->row = $this->result->fetch(PDO::FETCH_ASSOC)) {
				$this->data[$i] = $this->row;
				$i++;
			}
		}
		return $this->data;

	}

	public function member_del($group_id, $user_id) {
		$this->result = $this->PDO->prepare("
			DELETE FROM
				users_groups
			WHERE
				group_id = ?
			AND
				user_id = ?
		");
		try {
			$this->result->execute(func_get_args());
		} catch (Exception $e) {
			$this->error_msg(__FUNCTION__, $e->getMessage());
		}
	}

	public function member_add($group_id, $user_id) {
		if($user_id) {
			$this->result = $this->PDO->prepare("
			INSERT INTO
				users_groups
					(group_id, user_id)
			VALUES
				(?,?)
		");
			try {
				$this->result->execute(func_get_args());
			} catch (Exception $e) {
				$this->error_msg(__FUNCTION__, $e->getMessage());
			}
		}
	}

	public function group_link($group_id, $user_id) {
		$this->result = $this->PDO->prepare("
			DELETE FROM
				managers_groups
			WHERE
				group_id = ?
			AND
				user_id = ?;
			INSERT INTO
				managers_groups
					(group_id,user_id)
			VALUES
				(?,?)
		");
		try {
			$this->result->execute(array($group_id, $user_id, $group_id, $user_id));
		} catch (Exception $e) {
			$this->error_msg(__FUNCTION__, $e->getMessage());
		}
	}

	public function group_add($name, $manager_id) {
		$this->result = $this->PDO->prepare("
			INSERT IGNORE INTO
				groups
					(name)
			VALUES
				(?)
		");
		try {
			$this->result->execute(array($name));
		} catch (Exception $e) {
			$this->error_msg(__FUNCTION__, $e->getMessage());
		}
		$id = $this->PDO->lastInsertId();

		$this->group_link($id, $manager_id);
	}

	public function group_del($group_id) {
		$this->result = $this->PDO->prepare("
			DELETE FROM
				groups
			WHERE
				id = ?
		");
		try {
			$this->result->execute(array($group_id));
		} catch (Exception $e) {
			$this->error_msg(__FUNCTION__, $e->getMessage());
		}
	}

	public function group_clr($group_id) {
		$this->result = $this->PDO->prepare("
			DELETE FROM
				users_groups
			WHERE
				group_id = ?
		");
		try {
			$this->result->execute(array($group_id));
		} catch (Exception $e) {
			$this->error_msg(__FUNCTION__, $e->getMessage());
		}
	}

	public function get_text($name, $lang_id) {
		$this->result = $this->PDO->prepare("
			SELECT
				value
			FROM
				help
			WHERE
				name = ?
			AND
				language_id = ?
		");
		try {
			$this->result->execute(array($name, ($lang_id ? $lang_id : $this->lang_id)));
		} catch (Exception $e) {
			$this->error_msg(__FUNCTION__, $e->getMessage());
		}
		$this->data = NULL;
		$this->data = $this->result->fetch(PDO::FETCH_ASSOC);

		return $this->data['value'];
	}

	public function set_text($name, $text, $lang_id) {
		$this->result = $this->PDO->prepare("
			UPDATE
				`help`
			SET
				value = ?
			WHERE
				name = ?
			AND
				language_id = ?
		");
		try {
			$this->result->execute(array($text, $name, ($lang_id ? $lang_id : $this->lang_id)));
		} catch (Exception $e) {
			$this->error_msg(__FUNCTION__, $e->getMessage());
		}
	}

	public function upload_user_image($file, $user_id) {
		chdir('..');
		$allowed = array('jpg', 'jpeg', 'gif', 'png', 'bmp', 'tif', 'tiff');
		$temp = explode('.', $file['name']);
		$ext = end($temp);
		$uimage = $this->upload_name('users', $ext);
		if(in_array($ext, $allowed)) {
			if(!$file['error']) {
				if($file['size'] < (int)(ini_get('upload_max_filesize'))*2^20) {
					$f = move_uploaded_file($file['tmp_name'], 'upload/' . $uimage);
					$this->user_image_to_db('upload/' . $uimage, $user_id);
				}
			}
		}
	}

	public function write_user_image($image, $user_id) {
		$data = explode(".", $image);
		$ext = end($data);
		$file = fopen($image, "rb");
		$uimage = $this->upload_name('users', $ext);
		if($file) {
			$new = fopen($this->up_dir . $uimage, "wb");
			if($new) {
				while(!feof($file)) {
					fwrite($new, fread($file, 1024 * 8 ), 1024 * 8 );
				}
				$this->user_image_to_db('upload/' . $uimage, $user_id);
				fclose($new);
			}
			fclose($file);
		}
	}

	public function user_image_to_db($name, $user_id) {
		unlink($this->get_user_image($user_id));
		$this->result = $this->PDO->prepare("
			UPDATE
				users
			SET
				image = ?
			WHERE
				id = ?
		");
		try {
			$this->result->execute(array($name, $user_id));
		} catch (Exception $e) {
			$this->error_msg(__FUNCTION__, $e->getMessage());
		}
	}

	public function upload_name($dir, $ext) {
		return $dir . '/' . $this->unid(16) . '.' . $ext;
	}

	public function get_user_image($user_id) {
		$this->result = $this->PDO->prepare("
			SELECT
				image
			FROM
				users
			WHERE
				id = ?
		");
		try {
			$this->result->execute(array($user_id));
		} catch (Exception $e) {
			$this->error_msg(__FUNCTION__, $e->getMessage());
		}
		$this->data = NULL;
		$this->data = $this->result->fetch(PDO::FETCH_ASSOC);

		return $this->data['image'];
	}
}

?>
