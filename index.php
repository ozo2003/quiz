<?php

ob_start();
session_start();

define('DOCROOT', dirname(__FILE__));
function autoLoader($class){
	if(is_file(DOCROOT."/classes/".$class.".php")){
		require_once(DOCROOT."/classes/".$class.".php");
	} elseif(is_file(DOCROOT."/classes/system/".$class.".php")){
		require_once(DOCROOT."/classes/system/".$class.".php");
	} elseif(is_file(DOCROOT."/classes/api/".$class.".php")){
		require_once(DOCROOT."/classes/api/".$class.".php");
	} else {
		try {
			require_once(DOCROOT."/classes/".$class.".php");
		} catch(Exception $e){
			throw new Exception("Unable to load ".$class.".");
		}
	}
}
spl_autoload_register('autoLoader');
require_once(DOCROOT."/classes/configs/config.php");

function __($name){
	return Crypt::encrypt($name);
}

if(Input::post(__('action'), NULL)){
	// var_dump(HOST.SetPage::Post());
	header('Location: '.HOST.SetPage::Post());
}

$timer = Functions::Timer();

RainTPL::$tpl_dir = "templates/";
RainTPL::$cache_dir = "tmp/";

$main = new RainTPL();
$page = new RainTPL();

SetPage::DefaultsPre();

if (
	!file_exists('templates/'.SetPage::$getpage.'.html') || 
	!SetPage::$getpage || (
		!Menu::isPageAllowed(SetPage::$selfpage) && 
		SetPage::$selfpage != 'login'
	)
) {
	header('Location: '.HOST.'home');
}

if(SetPage::$getpage == 'logout'){
	session_destroy();
	header('Location: '.HOST.'home');
}

if(SetPage::$getpage == 'help'){
	header('Location: '.HOST.'help/help');
}

SetPage::DefaultsPost();

SetPage::Page(SetPage::$getpage);

SetPage::MainPage();

// var_dump($_SESSION);

?>
