<?php

class Role {

	public static function getRolesIds(){
		$query = "
			SELECT
				id
			FROM
				roles
			WHERE
				green = 1
		";
		return PDOc::getData($query);
	}

	public static function getRoleById($data){
		$query = "
			SELECT
				*
			FROM
				roles
			WHERE
				id = :role_id
		";
		return PDOc::getData($query,$data);
	}

	public static function getRolesNames($data){
		$data['role'] = $data['role_id'];
		$query = "
			SELECT
				id,
				name,
				active,
				level
			FROM
				roles
			WHERE
				manager_id = :user_id
			OR
				:role_id = 4
			OR
				id IN (2,:role)
			ORDER BY
				level, id
		";
		return PDOc::getData($query,$data);
	}

	public static function checkUserRole($data){
		$query = "
			SELECT
				IFNULL(ur.role_id, 1) AS role_id,
				r.level
			FROM
				users_roles ur
			LEFT JOIN
				roles r ON r.id = ur.role_id
			WHERE
				user_id = :user_id
		";
		$result = PDOc::getData($query,$data);
		if($result){
			return $result[0];
		} else {
			return 1;
		}
	}

}