<?php

class Chat {

	public static function getOnlineUsers($data = NULL){
		$query = "
			SELECT
				*
			FROM
				users_chat
			WHERE
				online = 1
		";

		return PDOc::getData($query, $data);
	}

	public static function getNewMessages($data){
		$query = "
			SELECT
				*
			FROM
				messages_chat
			WHERE
				id > :last_id
		";

		return PDOc::getData($query, $data);
	}

	public static function getLastMessageId($data){
		$query = "
			SELECT
				MAX(id) AS id
			FROM
				messages_chat
		";

		return PDOc::getData($query, $data);
	}

	public static function getUserName($data){
		$query = "
			SELECT
				username,
				password
			FROM
				users_chat
			WHERE
				session_id = :session_id
		";
		return PDOc::getData($query, $data);
	}

	public static function newUser($data){
		$result = self::getUserName($data);
		if($result[0]){
			return $result[0];
		} else {
			$query = "
				INSERT INTO
					users_chat
					 (username,online,session_id)
				VALUES
					(CONCAT('user_',FLOOR(1 + (RAND() * 9999))),1,:session_id)
			";
			PDOc::executeSQL($query, $data);
		}
		$result = self::getUserName($data);
		return $result[0];
	}

	public static function postMessage($data){
		$query = "
			INSERT INTO
				messages_chat
					(message,sender,send_time)
			VALUES
				(:message,:sender,NOW())
		";
		PDOc::getData($query, $data);
	}

	public static function checkUser($data){
		$ses = $data['session_id'];
		unset($data['session_id']);
		$query = "
			SELECT
				username
			FROM
				users_chat
			WHERE
				username = :username
			AND
				password = :password
		";
		$result = PDOc::getData($query,$data);
		if($result[0]){
			$data['session_id'] = $ses;
			self::setUserOnline($data);
			return TRUE;
		}
		return FALSE;
	}

	private static function setUserOnline($data){
		$query = "
			UPDATE
				users_chat
			SET
				online = 1,
				session_id = :session_id
			WHERE
				username = :username
			AND
				password = :password
		";
		PDOc::executeSQL($query,$data);
	}

	public static function newUserRegister($data){
		if(self::checkExisting($data['username'])){
			$query = "
				INSERT INTO
					users_chat
						(username,password,online,session_id)
				VALUES
					(:username,:password,1,:session_id)
			";
			PDOc::executeSQL($query,$data);
			return TRUE;
		}
		return FALSE;
	}

	private static function checkExisting($username){
		$data = array(
			'username' => $username
		);
		$query = "
			SELECT
				username
			FROM
				users_chat
			WHERE
				username = :username
		";
		$result = PDOc::getData($query,$data);
		if($result[0]){
			return FALSE;
		}
		return TRUE;
	}


	public static function setOfflineUser($data){
		$query = "
			UPDATE
				users_chat
			SET
				online = 0
			WHERE
				session_id = :session_id
		";
		PDOc::executeSQL($query, $data);
	}

	public static function deleteGuest($data){
		$query = "
			DELETE FROM
				users_chat
			WHERE
				session_id = :session_id
			AND
				password IS NULL
		";
		PDOc::executeSQL($query, $data);
	}

	public static function logoutUser($data){
		$query = "
			UPDATE
				users_chat
			SET
				online = 0,
				session_id = NULL
			WHERE
				session_id = :session_id
		";
		PDOc::executeSQL($query, $data);
	}

}