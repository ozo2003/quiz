<?php

class User {

	public static function checkUser($data){
		$data['email'] = $data['user'];
		$data['phone'] = $data['user'];
		$query = "
			SELECT
				u.id AS user_id,
				u.username,
				IFNULL(u.name,'') AS name,
				IFNULL(u.surname,'') AS surname,
				IFNULL(u.email,'') AS email,
				IFNULL(i.language_id,1) AS lang_id
			FROM
				users u
			LEFT JOIN
				ips i ON i.user_id = u.id
			WHERE
				(u.username = :user OR (u.email = :email AND confirmed IS NOT NULL) OR (u.phone = :phone AND confirmed_phone IS NOT NULL))
			AND
				u.password = :password
		";
		return PDOc::getData($query,$data);
	}

	public static function setAdminIp($data){
		$query = "
			UPDATE
				settings
			SET
				value = :ip
			WHERE
				name = 'adminip'
		";
		PDOc::executeSQL($query,$data);
	}

	public static function getUserImage($data){
		$query = "
			SELECT
				IFNULL(submitted_image,image) AS image
			FROM
				users
			WHERE
				id = :user_id
		";
		return PDOc::getData($query,$data);
	}

	public static function setUserImage($data){
		$query = "
			UPDATE
				users
			SET
				submitted_image = :name
			WHERE
				id = :user_id
		";
		PDOc::executeSQL($query,$data);
	}

	public static function changeUserPassword($data){
		$query = "
			UPDATE
				users
			SET
				submitted_password = :newpassword
			WHERE
				password = :password
			AND
				id = :user_id
		";
		PDOc::executeSQL($query,$data);
	}

	public static function getUserData($data){
		$query = "
			SELECT
				id,
				username,
				name,
				surname,
				email,
				phone,
				blocked
			FROM
				users
			WHERE
				id = :user_id
		";
		return PDOc::getData($query,$data);
	}

	public static function userExists($data){
		$query = "
			SELECT
				username
			FROM
				users
			WHERE
				id = :user_id
		";
		$result = PDOc::getData($query,$data);
		if($result){
			return $data['user_id'];
		} else {
			return NULL;
		}
	}

	public static function revokeUserImage($data){
		$query = "
			UPDATE
				users
			SET
				submitted_image = NULL
			WHERE
				id = :user_id
		";
		PDOc::executeSQL($query,$data);
	}

	public static function addUser($data){
		$query = "
			INSERT INTO
				users
					(username,password,name,surname,email,phone,creator_id)
			VALUES
				(:username,:password,:name,:surname,:email,:phone,:creator_id)
		";
		PDOc::executeSQL($query,$data);
	}

	public static function editUser($data){
		$query = "
			UPDATE
				users
			SET
				name = :name,
				surname = :surname,
				email = :email,
				phone = :phone,
				blocked = :blocked,
				image = IFNULL(submitted_image, image),
				submitted_image = NULL,
				password = IFNULL(submitted_password, password),
				submitted_password  = NULL
			WHERE
				id = :user_id
		";
		PDOc::executeSQL($query,$data);
	}

	public static function deleteUser($data){
		$query = "
			DELETE FROM
				users
			WHERE
				id = :user_id
		";
		PDOc::executeSQL($query,$data);
	}

}