<?php

class Menu {

	public static function buildMenu($data){
		$query = "
			SELECT
				m.`".$data['lang_id']."` AS name,
				m.name AS lower,
				CASE WHEN m.`order` = 0 THEN 999 ELSE m.`order` END AS morder,
				r.level
			FROM
				menus m
			LEFT JOIN
				roles r ON r.id = m.role_id
			WHERE
				m.visible = 1
			AND
				m.role_id = :role_id
			ORDER BY
				morder
			LIMIT
				6
		";
		unset($data['lang_id']);
		return PDOc::getData($query,$data);
	}

	public static function buildMenuGreen($data){
		$query = "
			SELECT
				m.`".$data['lang_id']."` AS name,
				m.name AS lower,
				CASE WHEN m.`order` = 0 THEN 999 ELSE m.`order` END AS morder,
				r.level
			FROM
				menus_green m
			LEFT JOIN
				roles r ON r.id = m.role_id
			WHERE
				m.visible = 1
			AND
				m.role_id = :role_id
			ORDER BY
				morder
		";
		unset($data['lang_id']);
		return PDOc::getData($query,$data);
	}

	public static function getMenusByRoleId($data){
		$query = "
			SELECT DISTINCT
				m.id,
				m.name,
				m.`order`,
				m.visible,
				CASE WHEN (m.`order` = 0 OR m.visible = 0) THEN 999 ELSE m.`order` END AS `ord`,
				m.`1` AS english,
				m.`2` AS latvian
			FROM
				menus m
			WHERE
				m.role_id = :role_id
			ORDER BY
				`ord`, name
		";
		return PDOc::getData($query,$data);
	}

	public static function saveMenu($data){
		$query = "
			UPDATE
				menus
			SET
				name = :name,
				`order` = :order,
				visible = :visible,
				`1` = :a,
				`2` = :b
			WHERE
				id = :menu_id
		";
		PDOc::executeSQL($query,$data);
	}
	
}