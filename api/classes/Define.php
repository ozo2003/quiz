<?php

class Define {

	public static function getLangId($data){
		$query = "
			SELECT
				language_id
			FROM
				ips
			WHERE
				ip = :ip
		";
		return PDOc::getData($query, $data);
	}

	public static function getLangIdByName($data){
		$query = "
			SELECT
				id
			FROM
				languages
			WHERE
				language = :name
		";
		return PDOc::getData($query, $data);
	}

	public static function setLangId($data){
		$query = "
			UPDATE
				ips
			SET
				language_id = :lang_id
			WHERE
				ip = :ip
		";
		PDOc::executeSQL($query,$data);
	}

	public static function setText($data){
		$query = "
			UPDATE
				`help`
			SET
				value = :value
			WHERE
				name = :name
			AND
				language_id = :lang_id
		";
		PDOc::executeSQL($query,$data);
	}

	public static function getText($data){
		$query = "
			SELECT
				value
			FROM
				help
			WHERE
				name = :name
			AND
				language_id = :lang_id
		";
		return PDOc::getData($query,$data);
	}

	public static function getLoopValues($data){
		$query = "
			SELECT
				name,
				value
			FROM
				defines
			WHERE
				`loop` = 1
			AND
				language_id = :lang_id
			AND
				type_id <> 5
			ORDER BY
				name
		";
		return PDOc::getData($query,$data);
	}

	public static function getLoopValuesGreen($data){
		$query = "
			SELECT
				name,
				value
			FROM
				defines
			WHERE
				`loop` = 1
			AND
				language_id = :lang_id
			AND
				type_id = 5
			ORDER BY
				name
		";
		return PDOc::getData($query,$data);
	}

	public static function getDefines($data){
		$query = "
			SELECT
				d.value as name,
				ds.function as function,
				d.comment
			FROM
				definable_settings ds
			LEFT JOIN
				defines d ON d.name = ds.name
			WHERE
				d.language_id = :lang_id
			AND
				ds.`type` = :name
			ORDER BY
				d.value
		";
		return PDOc::getData($query,$data);
	}

	public static function getValues($data){
		$query = "
			SELECT
				id,
				name,
				value,
				comment
			FROM
				defines
			WHERE
				language_id = :lang_id
			AND
				type_id = 1
			ORDER BY
				name
		";
		return PDOc::getData($query,$data);
	}

	public static function setValue($data){
		$query = "
			UPDATE
				defines
			SET
				value = :value
			WHERE
				name = :name
			AND
				id = :id
		";
		PDOc::executeSQL($query,$data);
	}

	public static function Translate($data){
		$query = "
			SELECT
				value
			FROM
				defines
			WHERE
				name = :name
			AND
				language_id = :lang_id
		";
		return PDOc::getData($query,$data);
	}

	public static function getTextNames(){
		$query = "
			SELECT DISTINCT
				name
			FROM
				`help`
			ORDER BY
				name ASC
		";
		return PDOc::getData($query);
	}

	public static function helpIdByName($data){
		$query = "
			SELECT
				id
			FROM
				help
			WHERE
				name = :name
			AND
				language_id = :lang_id
		";
		return PDOc::getData($query,$data);
	}

}