<?php

class Manage {

	public static function getAllUsers($data){
		$data['id'] = $data['user_id'];
		$data['user'] = $data['user_id'];
		$data['user_id2'] = $data['user_id'];
		$query = "
			SELECT
				u.id,
				u.username,
				IFNULL(u.name,'') AS name,
				IFNULL(u.surname,'') AS surname,
				IFNULL(u.email,'') AS email,
				IFNULL(u.phone,'') AS phone,
				d.value,
				r.id AS role_id,
				u.blocked,
				r.manager_id,
				u.creator_id
			FROM
				users u
			LEFT JOIN
				users_roles ur ON ur.user_id = u.id
			LEFT JOIN
				roles r ON r.id = ur.role_id
			LEFT JOIN
				defines d ON d.name = r.name
			WHERE
				d.language_id = :lang_id
			AND (
					1 = :id
				OR
					r.manager_id = :user_id
				OR
					u.id = :user
				OR
					4 = :role_id
				OR
					u.creator_id = :user_id2
			)

		";
		return PDOc::getData($query,$data);
	}

	public static function getRoles(){
		$query = "
		SELECT
			r.*,
			(SELECT value FROM defines d WHERE d.name = r.name AND language_id = 1) AS english,
			(SELECT value FROM defines d WHERE d.name = r.name AND language_id = 2) AS latvian
		FROM
			roles r
		WHERE
			r.`delete` <> 1
		ORDER BY
			r.id
		";
		return PDOc::getData($query);
	}

	public static function getRolesList($data){
		$query = "
			SELECT
				r.id,
				d.value,
				r.manager_id
			FROM
				roles r
			LEFT JOIN
				defines d ON r.name = d.name
			WHERE
				d.language_id = :lang_id
			AND
				r.`delete` = 0
			AND
				r.id <> 1
			ORDER BY
				r.id ASC
		";
		return PDOc::getData($query,$data);
	}
}