<?php

define('DOCROOT', dirname(__FILE__));
function autoLoader($class){
	if(is_file(DOCROOT."/system/global/".$class.".php")){
		require_once(DOCROOT."/system/global/".$class.".php");
	} elseif(is_file(DOCROOT."/system/".$class.".php")){
		require_once(DOCROOT."/system/".$class.".php");
	} elseif(is_file(DOCROOT."/classes/".$class.".php")){
		require_once(DOCROOT."/classes/".$class.".php");
	} else {
		try {
			require_once(DOCROOT."/".$class.".php");
		} catch(Exception $e){
			throw new Exception("Unable to load ".$class.".");
		}
	}
}
spl_autoload_register('autoLoader');

require_once("configs/config.php");

try {
	$PDO = new PDOc(DBName,DBUser,DBPass,DBHost);
	$pdo = $PDO->pdo;
	$request = Input::post('request',NULL);
	$command = Commands::Parse($request);
	echo $command;
} catch(Exception $e){
	echo $e->getMessage();
}