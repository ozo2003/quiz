<?php

class Commands {

	public static function Parse($command){
		if($command && self::json_validate($command)){
			$post = json_decode($command,true);
		} else {
			$post = array('error' => 'JSON Input Error');
		}
		return json_encode(array('data' => self::commandSwitch($post)));
	}

	private static function json_validate($json, $assoc_array = FALSE) {
	    $result = json_decode($json, $assoc_array);
	    switch (json_last_error()) {
	        case JSON_ERROR_NONE:
	            $error = FALSE;
	            break;
	        default:
	            $error = TRUE;
	            break;
	    }
	    if($error) {
	        return FALSE;
	    }
	    return TRUE;
	}

	private static function commandSwitch($post){
		$command = (isset($post['command']) ? $post['command'] : NULL);
		$token = (isset($post['token']) ? $post['token'] : NULL);
		$data = (isset($post['data']) ? $post['data'] : NULL);
		$class = (isset($post['class']) ? $post['class'] : NULL);
		if(!$command){
			return Input::returnError('Unkonown Command');
		} 
		// if(!$token){
		// 	return Input::returnError('No Token Provided');
		// }
		switch($class){
			case 'Define': return self::defineSwitch($command, $data); break;
			case 'Mail': return self::mailSwitch($command, $data); break;
			case 'Manage': return self::manageSwitch($command, $data); break;
			case 'Menu': return self::menuSwitch($command, $data);	break;
			case 'Quiz': return self::quizSwitch($command, $data); break;
			case 'Role': return self::roleSwitch($command, $data); break;
			case 'User': return self::userSwitch($command, $data); break;
			case 'Chat': return self::chatSwitch($command, $data); break;
			default: return Input::returnError('Unkonown Command');	break;
		}
	}

	private static function returnError($text){
		return array('error' => $text);
	}

	public static function generateToken($data){
			
	}

	private static function menuSwitch($command, $data = NULL){
		switch($command){
			case 'buildMenu': return Menu::buildMenu($data); break;
			case 'buildMenuGreen': return Menu::buildMenuGreen($data); break;
			case 'getMenusByRoleId': return Menu::getMenusByRoleId($data); break;
			case 'saveMenu': Menu::saveMenu($data); return TRUE; break;
		}
	}	

	private static function defineSwitch($command, $data = NULL){
		switch($command){
			case 'getLangId': return Define::getLangId($data); break;
			case 'setLangId': Define::setLangId($data); return TRUE; break;
			case 'setText': Define::setText($data); return TRUE; break;
			case 'getText': return Define::getText($data); break;
			case 'getLoopValues': return Define::getLoopValues($data); break;
			case 'getLoopValuesGreen': return Define::getLoopValuesGreen($data); break;
			case 'getDefines': return Define::getDefines($data); break;
			case 'getLangIdByName': return Define::getLangIdByName($data); break;
			case 'getValues': return Define::getValues($data); break;
			case 'setValue': Define::setValue($data); return TRUE; break;
			case 'Translate': return Define::Translate($data); break;
			case 'getTextNames': return Define::getTextNames(); break;
			case 'helpIdByName': return Define::helpIdByName($data); break;
		}
	}

	private static function userSwitch($command, $data = NULL){
		switch($command){
			case 'checkUser': return User::checkUser($data); break;
			case 'setAdminIp': User::setAdminIp($data); return TRUE; break;
			case 'getUserImage': return User::getUserImage($data); break;
			case 'setUserImage': User::setUserImage($data); return TRUE; break;
			case 'changeUserPassword': User::changeUserPassword($data); return TRUE; break;
			case 'getUserData': return User::getUserData($data); break;
			case 'userExists': return User::userExists($data); break;
			case 'revokeUserImage': User::revokeUserImage($data); return TRUE; break;
			case 'addUser': User::addUser($data); return TRUE; break;
			case 'editUser': User::editUser($data); return TRUE; break;
			case 'deleteUser': User::deleteUser($data); return TRUE; break;
		}
	}

	private static function manageSwitch($command, $data = NULL){
		switch($command){
			case 'getAllUsers': return Manage::getAllUsers($data); break;
			case 'getRoles': return Manage::getRoles(); break;
			case 'getRolesList': return Manage::getRolesList($data); break;
		}
	}

	private static function roleSwitch($command, $data = NULL){
		switch($command){
			case 'getRolesIds': return Role::getRolesIds(); return TRUE; break;
			case 'getRoleById': return Role::getRoleById($data); break;
			case 'getRolesNames': return Role::getRolesNames($data); break;
			case 'checkUserRole': return Role::checkUserRole($data); break;
		}
	}

	private static function quizSwitch($command, $data = NULL){
		switch($command){

		}
	}

	private static function mailSwitch($command, $data = NULL){
		switch($command){

		}
	}

	private static function chatSwitch($command, $data = NULL){
		switch($command){
			case 'getOnlineUsers': return Chat::getOnlineUsers($data); break;
			case 'getNewMessages': return Chat::getNewMessages($data); break;
			case 'getLastMessageId': return Chat::getLastMessageId($data); break;
			case 'newUser': return Chat::newUser($data); break;
			case 'postMessage': Chat::postMessage($data); break;
			case 'checkUser': return Chat::checkUser($data); break;
			case 'newUserRegister': return Chat::newUserRegister($data); break;
			case 'setUserOffline': Chat::setUserOffline($data); break;
			case 'deleteGuest': Chat::deleteGuest($data); break;
			case 'logoutUser': Chat::logoutUser($data); break;
		}
	}

}