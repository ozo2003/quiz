<?php

class Functions {

	public static function isArrayInside($array){
	    foreach($array as $value){
	        if(is_array($value)) {
	          return true;
	        }
	    }
	    return false;
	}

	public static function unId($len = 5) {
		$str = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890';
		return substr(str_shuffle($str), 0, $len);
	}

	public static function checkEmail($email) {
		$regex = "/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/";
		return preg_match($regex, $email) ? 1 : 0;
	}

}