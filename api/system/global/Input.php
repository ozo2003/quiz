<?php

class Input {

	public static function get($key, $default = NULL){
		$val = ((empty($_GET) || empty($_GET[$key])) ? $default : $_GET[$key]);
		return $val;
	}

	public static function post($key, $default = NULL){
		$val = ((empty($_POST) || empty($_POST[$key])) ? $default : $_POST[$key]);
		return $val;
	}

	public static function unid($len = 5) {
		$str = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890';
		return substr(str_shuffle($str), 0, $len);
	}

	public static function error_msg($function, $error) {
		return self::returnError($function . ' : ' . $error);
	}

	public static function returnError($text){
		return array('error' => $text);
	}

	public static function file($key, $default = NULL){
		$val = ((empty($_FILE) || empty($_FILE[$key])) ? $default : $_FILE[$key]);
		return $val;
	}

}