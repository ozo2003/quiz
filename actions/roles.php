<?php

session_start();
require '../includes/Settings.php';
$db = new Settings();

$keys = array_keys($_POST);
$data = $_POST;
$checks = array('active', 'delete', 'manager');

for($i = 0; $i < count($checks); $i++) {
	$data[$checks[$i]] = (isset($data[$checks[$i]]) ? 1 : 0);
}

$db->update_roles($data);

header('Location: ' . $db->root . 'manage/roles');

?>