<?php

session_start();
require '../includes/Settings.php';
$db = new Settings();

$data = $_POST;
$data['email'] = ((strlen($data['email']) > 0 && $db->check_email($data['email'])) || strlen($data['email']) == 0 ? $data['email'] : '');

$data['role'] = 2;
$result = $db->add_user($data);
if($db->role_id != 1) {
	if($result) {
		header('Location: ' . $db->root . 'manage/users');
	} else {
		header('Location: ' . $db->root . 'manage/users/add');
	}
} else {
	if($result) {
		header('Location: ' . $db->root . 'home');
	} else {
		header('Location: ' . $db->root . 'register');
	}
}

?>
