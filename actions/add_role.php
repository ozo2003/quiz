<?php

session_start();

require '../includes/Settings.php';
$db = new Settings();

$checks = array('active', 'manager');
$warn = array('role', 'english', 'manager_id');
$data = $_POST;
for($i = 0; $i < count($checks); $i++) {
	$data[$checks[$i]] = (isset($data[$checks[$i]]) ? 1 : 0);
}

$error = FALSE;
for($j = 0; $j < count($warn); $j++) {
	if(!isset($data[$warn[$i]])) {
		$error = TRUE;
		break;
	}
}

if(!$error) {
	$db->insert_role($data);
}

header('Location: ' . $db->root . 'manage/roles');

?>
