<?php

require '../includes/Settings.php';
$db = new Settings();

if(isset($_POST['delete'])) {
	$db->user_delete($_POST['id']);
} else {

	$data = $_POST;
	$data['blocked'] = (isset($_POST['blocked']) ? 1 : 0);
	$error = ((strlen($data['email']) > 0 && $db->check_email($data['email'])) || strlen($data['email']) == 0 ? 0 : 1);

	if($error) {
		unset($data['email']);
	}

	$db->update_user_tb($data);
}
header('Location: '.$db->root . 'manage/users');

?>
